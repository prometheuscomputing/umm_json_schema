require_relative 'umm_json_schema/meta_info'
require_relative 'umm_json_schema/schema_builder'

module UmmJsonSchema
  # NOTE: this method is deprecated in favor of generate_from_dsl_file
  def self.generate_from_marshal_file(umm_marshal_file, options = {})
    builder = SchemaBuilder.new(options)
    project = UmlMetamodel.from_marshal_file(umm_marshal_file)
    builder.generate_schema(project)
  end  
  def self.generate_from_dsl_file(umm_dsl_file, options = {})
    project = UmlMetamodel.from_dsl_file(umm_dsl_file, :suppress_warnings => false)
    generate_from_project(project, options)
  end
  def self.generate_from_project(project, options = {})
    SchemaBuilder.new(options).generate_schema(project)
  end
end