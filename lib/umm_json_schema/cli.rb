# FIXME: this file is mostly copied from UmmDocumentation
module UmmJsonSchema
  class CLI
    SUCCESS = 0
    NO_DSL_FILE_SPECIFIED_ERROR = 1
    DSL_FILE_NOT_FOUND_ERROR = 2
    INVALID_OPTION_ERROR = 3
    INVALID_TEMPLATE_ERROR = 4
    attr_accessor :stdout
    attr_accessor :stderr
    
    def self.run!(args, stdout = $stdout, stderr = $stderr)
      self.new(stdout, stderr).execute!(args)
    end
    
    def initialize(stdout = $stdout, stderr = $stderr)
      @stdout = stdout
      @stderr = stderr
    end
    
    # Executes the command, returning an exit code of zero for success or non-zero for failure
    def execute!(args)
      options, opt_parser = parse_options!(args)
      return options[:exit] if options[:exit]
      dsl_file = File.expand_path(args[0])
      unless File.exists?(dsl_file)
        @stderr.puts "invalid dsl file: #{dsl_file}"
        @stdout.puts opt_parser
        return DSL_FILE_NOT_FOUND_ERROR
      end
      
      schema = UmmJsonSchema.generate_from_dsl_file(dsl_file, options)
      
      if options[:output_file]
        File.open(options[:output_file], 'w+'){|f| f.puts schema }
      else
        @stdout.puts schema
      end
      SUCCESS
    end
    
    # Parse passed option flags into hash, removing flags from args
    # If -h (or no args) is passed, shows a usage message and exits
    # If -v is passed, shows the version and exits
    def parse_options!(args)
      options = {}
      # Parse options removing flags from args
      opt_parser = options_parser(options)
      begin
        opt_parser.parse!(args)
      rescue OptionParser::InvalidOption => e
        @stderr.puts e.message
        @stdout.puts opt_parser
        options[:exit] = INVALID_OPTION_ERROR
      end
      
      if args.empty? && !options[:exit] # All args were options, no dsl_file was specified. Print usage and set exit option
        @stderr.puts "You must specify a DSL file"
        @stdout.puts opt_parser
        options[:exit] = NO_DSL_FILE_SPECIFIED_ERROR
      end
      
      # Return options and parser (used to display usage if needed)
      [options, opt_parser]
    end
    
    def options_parser(options = {})
      OptionParser.new do |opts|
        opts.banner = "Usage: umm_json_schema umm_dsl_file [OPTIONS]"
  
        opts.on('-o', '--output-file FILEPATH', "File to output schema. Prints to terminal if not specified") do |o|
          options[:output_file] = File.expand_path(o)
        end
        
        opts.on('-r', '--references REFERENCES', "How to represent references in JSON schema. Allowed values are 'id_attributes' and 'ref_objects' (default).") do |r|
          options[:references] = r.to_sym
        end
        
        opts.on('-v', '--version', "Show version information") do
          @stdout.puts "umm_json_schema v#{UmmJsonSchema::VERSION}"
          options[:exit] = SUCCESS
        end
        
        opts.on('-h', '--help', "Show this message") do
          @stdout.puts opts
          options[:exit] = SUCCESS
        end
      end
    end
  end
end