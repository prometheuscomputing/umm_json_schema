# TODO: this should be in its own project -SD
require 'json_graph'
module JsonSchemaMetamodel
  # Class object will be represented as a Ruby Hash
  # class Object; end
  
  # Class array will be represented as a Ruby Array
  # class Array; end
  
  class Schema # < Object

    def initialize
      self.additional_attributes = {}
      # @exclusiveMaximum = false
      # @exclusiveMinimum = false
      # @uniqueItems = false
    end

    include JSON_Graph::Serializeable
    # $ref - type: string, format: json-pointer
    attr_accessor :ref
    # type: string, format: uri
    attr_accessor :id
    # Schema property - This is only used for the root object in a JSON Schema
    # $schema - type: string, format: uri
    attr_accessor :schema
    # type: string
    attr_accessor :title
    # type: string
    attr_accessor :description
    # default: {}
    attr_accessor :default
    
    # attributes for type: number/integer:
    # type: number, minimum: 0, exclusiveMinimum: true
    attr_accessor :multipleOf
    # type: number
    attr_accessor :maximum
    # type: boolean, default: false
    attr_accessor :exclusiveMaximum
    # type: number
    attr_accessor :minimum
    # type: boolean, default: false
    attr_accessor :exclusiveMinimum
    
    # attributes for type: string:
    # $ref: #/definitions/positiveInteger
    attr_accessor :maxLength
    # $ref: #/definitions/positiveIntegerDefault0
    attr_accessor :minLength
    # type: string, format: regex
    # Regex should be a valid regular expression, according to the ECMA 262 regular expression dialect
    attr_accessor :pattern
    
    # attributes for type: array:
    # Whether or not additional items are allowed in the array.
    # NOTE: only valid when items is an array
    # NOTE: This can also be a schema as per the spec, although this doesn't seem to be common usage
    # anyOf: [type: boolean, $ref: #], default: {}
    attr_accessor :additionalItems
    # Defines either:
    # * a schema which every element in the array must match
    # * or an array of schemas which each element in the array must match sequentially
    # anyOf: [$ref: #, $ref: #/definitions/schemaArray], default: {}
    attr_accessor :items
    # $ref: #/definitions/positiveInteger
    attr_accessor :maxItems
    # $ref: #/definitions/positiveIntegerDefault0
    attr_accessor :minItems
    # type: boolean, default: false
    attr_accessor :uniqueItems
    
    # attributes for type: object(hash):
    # $ref: #/definitions/positiveInteger
    attr_accessor :maxProperties
    # $ref: #/definitions/positiveIntegerDefault0
    attr_accessor :minProperties
    # Which properties are required
    # $ref: #/definitions/stringArray
    attr_accessor :required
    # Wether or not additional properties are allowed, or
    # constraints on any specified additional properties
    # anyOf: [type: boolean, $ref: #], default: {}
    attr_accessor :additionalProperties
    # type: object, additionalProperties: {$ref: #}, default: {}
    attr_accessor :definitions
    # type: object, additionalProperties: {$ref: #}, default: {}
    attr_accessor :properties
    # Specifies constraints on additionalProperties whose names match the specified patterns
    # type: object, additionalProperties: {$ref: #}, default: {}
    attr_accessor :patternProperties
    # essentially constraints. Can be:
    # * property constraints - If a specified property is present, require an array of other properties to be present.
    # * schema constraints - If a specified property is present, extends the schema to have other constraints/properties.
    # type: object, additionalProperties: {anyOf: [$ref: #, $ref: #/definitions/stringArray]}
    attr_accessor :dependencies
    
    # enum attribute works with string, numerics, null, others?:
    # Restricts the value of the property to one of the specified values
    # type: array, minItems: 1, uniqueItems: true
    attr_accessor :enum
    
    # anyOf: [{ref: #/definitions/simpleTypes}, {type: array, items: {$ref: #/definitions/simpleTypes}, minItems: 1, uniqueItems: true}]
    # Allowed types are: ['array', 'boolean', 'integer', 'null', 'number', 'object', 'string']
    # OR an array containing a unique subset of those types (the array must also have at least one member).
    attr_accessor :type
    
    # Bizarrely, this is not a part of the spec, although it is used within the definition for JSON Schema itself
    # They seem to only be used to constrain type: string
    # Known 'recognized' formats are ['regex', 'uri', 'date-time', 'email', 'hostname', 'ipv4', 'ipv6']
    # There is an additional 'json-pointer' format for which JSON Schema does not have a recognized format, but is used 
    #   by the $ref property (See https://tools.ietf.org/html/rfc6901)
    attr_accessor :format
    
    # Combining schemas:
    # Must be valid against all of the subschemas
    # $ref: #/definitions/schemaArray
    attr_accessor :allOf
    # Must be valid against any of the subschemas
    # $ref: #/definitions/schemaArray
    attr_accessor :anyOf
    # Must be valid against exactly one of the subschemas
    # $ref: #/definitions/schemaArray
    attr_accessor :oneOf
    # Must not match the specified subschema
    # $ref: #
    attr_accessor :not

    attr_accessor :additional_attributes
    
    def self.add_additional_attribute(attribute)
     JSON_SERIALIZATION_SCHEME[Schema][attribute] = "additional_attributes.[](:#{attribute})"
    end
    
  end
  
  JSON_SERIALIZATION_SCHEME = {
    Schema => {
      :'$ref' => 'ref',
      :id => nil,
      :'$schema' => 'schema',
      :title => nil,
      :description => nil,
      :default => nil,
      :multipleOf => nil,
      :maximum => nil,
      :exclusiveMaximum => nil,
      :minimum => nil,
      :exclusiveMinimum => nil,
      :maxLength => nil,
      :minLength => nil,
      :pattern => nil,
      :additionalItems => nil,
      :items => nil,
      :maxItems => nil,
      :minItems => nil,
      :uniqueItems => nil,
      :maxProperties => nil,
      :minProperties => nil,
      :required => nil,
      :additionalProperties => nil,
      :definitions => nil,
      :properties => nil,
      :patternProperties => nil,
      :dependencies => nil,
      :enum => nil,
      :type => nil,
      :format => nil,
      :allOf => nil,
      :anyOf => nil,
      :oneOf => nil,
      :not => nil
    }
  }
  
end