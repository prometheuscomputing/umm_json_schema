module UmmJsonSchema
  # Maps the UmlMetamodel primitives to their JSON Schema equivalents
  # Allowed JSON Schema types are: ['array', 'boolean', 'integer', 'null', 'number', 'object', 'string']
  # Known 'recognized' string formats are ['regex', 'uri', 'date-time', 'email', 'hostname', 'ipv4', 'ipv6']
  def self.uml_json_type_mapping
    {
      UmlMetamodel::PRIMITIVES[:string] => {:type => 'string'},
      UmlMetamodel::PRIMITIVES[:integer] => {:type => 'integer'},
      UmlMetamodel::PRIMITIVES[:boolean] => {:type => 'boolean'},
      UmlMetamodel::PRIMITIVES[:null] => {:type => 'null'},
      UmlMetamodel::PRIMITIVES[:real] => {:type => 'number'},
      UmlMetamodel::PRIMITIVES[:byte] => {:type => 'string', :format => 'byte'},
      UmlMetamodel::PRIMITIVES[:char] => {:type => 'string', :format => 'char'},
      UmlMetamodel::PRIMITIVES[:date] => {:type => 'string', :format => 'date'},
      UmlMetamodel::PRIMITIVES[:float] => {:type => 'number'}, # TODO: consider adding :format => 'float'
      UmlMetamodel::PRIMITIVES[:unlimited_natural] => {:type => 'integer'},
      # Additional primitives not defined in MagicDraw's UML primitives
      UmlMetamodel::PRIMITIVES[:datetime] => {:type => 'string', :format => 'date-time'},
      UmlMetamodel::PRIMITIVES[:time] => {:type => 'string', :format => 'time'},
      UmlMetamodel::PRIMITIVES[:uri] => {:type => 'string', :format => 'uri'},
      UmlMetamodel::PRIMITIVES[:regular_expression] => {:type => 'string', :format => 'regex'}
    }
  end

  def self.json_uml_type_mapping
    # Note that this mapping excludes JSON Schema types: 'array' and 'object'
    # This mapping also includes support for non-standard string formats: byte, char, date, time
    {
      {:type => 'boolean'} => UmlMetamodel::PRIMITIVES[:boolean],
      {:type => 'integer'} => UmlMetamodel::PRIMITIVES[:integer],
      {:type => 'null'} => UmlMetamodel::PRIMITIVES[:null],
      {:type => 'number'} => UmlMetamodel::PRIMITIVES[:float],
      {:type => 'string'} => UmlMetamodel::PRIMITIVES[:string],
      {:type => 'string', :format => 'regex'} => UmlMetamodel::PRIMITIVES[:regular_expression],
      {:type => 'string', :format => 'uri'} => UmlMetamodel::PRIMITIVES[:uri],
      {:type => 'string', :format => 'date-time'} => UmlMetamodel::PRIMITIVES[:datetime],
      {:type => 'string', :format => 'email'} => UmlMetamodel::PRIMITIVES[:string],
      {:type => 'string', :format => 'hostname'} => UmlMetamodel::PRIMITIVES[:string],
      {:type => 'string', :format => 'ipv4'} => UmlMetamodel::PRIMITIVES[:string],
      {:type => 'string', :format => 'ipv6'} => UmlMetamodel::PRIMITIVES[:string],
      # Non-standard additions
      {:type => 'string', :format => 'byte'} => UmlMetamodel::PRIMITIVES[:byte],
      {:type => 'string', :format => 'char'} => UmlMetamodel::PRIMITIVES[:char],
      {:type => 'string', :format => 'date'} => UmlMetamodel::PRIMITIVES[:date],
      {:type => 'string', :format => 'time'} => UmlMetamodel::PRIMITIVES[:time]
    }
  end

  def self.json_class_name(uml_class)
    uml_class.qualified_name.split('::').join('.')
  end
end