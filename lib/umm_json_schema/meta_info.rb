module UmmJsonSchema
  
  # For more information about meta_info.rb, please see project Foundation, lib/Foundation/meta_info.rb
  
  # SUGGESTION: Treat "Optional" as meaning "can be nil", and define all constants, even if the value happens to be nil.
  
  # Required String
  GEM_NAME = "umm_json_schema"
  # Required String
  VERSION = '1.5.4'
  # Optional String or Array of Strings
  AUTHORS = ["Sam Dana", "Julia Dana"]
  # Optional String or Array of Strings
  EMAILS = ["s.dana@prometheuscomputing.com", "j.dana@prometheuscomputing.com"]
  # Optional String
  HOMEPAGE = nil
  # Required String
  SUMMARY = %q{Generates a JSON Schema based off a UML Metamodel.}
  # Optional String
  DESCRIPTION = %q{Given a UML Metamodel, expressed using the uml_metamodel API, generates a JSON schema according to the specified options.}
  
  # Required Symbol
  # This specifies the language the project is written in (not including the version, which is in LANGUAGE_VERSION).
  # A project should only have one language (not including, for example DSLs such as templating languages).
  # If a project has more than one language (not including DSLs), it should be split.
  # TEMPORARY EXCEPTION: see :frankenstein choice below.
  # The reason for a single language is that mixing up languages in one project complicates packaging, deployment, metrics, directory structure, and many other aspects of development.
  # Choices are currently:
  #   * :ruby (implies packaging as gem - contains ZERO java code)
  #   * :java (implies packaging as jar, ear, war, sar, etc (depending on TYPE) - contains ZERO ruby code, with exception of meta_info.rb)
  #   * :frankenstein (implies packaging as gem - contains BOTH ruby and java code - will probably deprecate this in favor of two separate projects)
  LANGUAGE = :ruby
  # This differs from Runtime version - this specifies the version of the syntax of LANGUAGE
  LANGUAGE_VERSION = ['> 1.9.2']
  # This is different from aGem::Specification.platform, which appears to be concerned with OS.
  # This defines which implentation of Ruby, Java, etc can be used.
  # Required Hash, in same format as DEPENDENCIES_RUBY.
  # The version part is used by required_ruby_version
  # Allowable keys depend on LANGUAGE. They are in VALID_<language.upcase>_RUNTIMES
  RUNTIME_VERSIONS = {
    :mri => ['>= 1.9.2'],
    :jruby => ['> 1.7']
  }
  # Required Symbol
  # Choices are currently:
  #   * :library - reusable functionality, not intended to stand alone
  #   * :utility - intended for use on command line
  #   * :web_app - an application that uses a web browser for it's GUI
  #   * :service - listens on some port. May include command line tools to manage the server.
  #   * :gui - has a Swing, Fox, WXwidget, etc GUI
  TYPE = :library
  # Required when TYPE is not :utility or :library.
  # Specifies what to invoke when the user double clicks on a packaged application.
  # Note also ".platypus" alternative.
  # This is a String path, relative to either bin or lib. If present in both, bin is used.
  # If in bin, this will be invoked through the operating system.
  LAUNCHER = nil
  
  # Optional Hashes - if nil, presumed to be empty.
  # There may be additional dependencies, for platforms (such as :maglev) other than :mri and :jruby
  # In the case of JRuby platform Ruby code that depends on a third party Java jar, where do we specify that?
  
  DEPENDENCIES_RUBY = {
    :uml_metamodel => '~> 3.0',
    :json_graph => '~> 2.0',
    :activesupport => '~> 5.1',
    :'json-schema' => '~> 2.8'
  }
  DEPENDENCIES_MRI = { }
  DEPENDENCIES_JRUBY = { }
  DEVELOPMENT_DEPENDENCIES_RUBY = {:rspec => '~> 3.6', :simplecov => '~> 0.14'}
  DEVELOPMENT_DEPENDENCIES_MRI = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { }
  
  # An Array of strings that YARD will interpret as regular expressions of files to be excluded.
  YARD_EXCLUDE = []
end