require 'uml_metamodel'
require 'json'

# Provides UmlMetamodel.fix_project_elements!(project) method to fix XSD Primitive and Gui_Builder_Profile references
require 'uml_metamodel/fixes'
# Required for String#camelize method
require 'active_support/inflector'

require_relative 'element_extensions'
require_relative 'json_schema_metamodel'
require_relative 'uml_json_mapping'

module UmmJsonSchema
  class SchemaBuilder
    attr_accessor :options

    def initialize(options = {})
      # Set options. Options include
      # :references =>
      #   - :id_attributes - Generate references as explicit id attributes
      #   - :ref_objects - Generate references as objects with '@ref' property (default)
      # :id_suffix - The string suffix to append to property names when using 'id_attributes' references mode. Defaults to 'Id'
      # :id_suffix_plural - The string suffix to append to plural reference properties. Defaults to 'Ids'
      defaults = {
        :references => :ref_objects,
        :id_suffix => 'Id',
        :id_suffix_plural => 'Ids'
      }
      @options = defaults.merge(options)
      JsonSchemaMetamodel::Schema.add_additional_attribute(:refTypes)
    end

    def generate_schema(project)
      # Fix problems with project elements that were not translated properly
      # This removes references to XML primitives in the "Primitives (Class based)" and replaces them with UML primitives
      # TODO: some of this should be handled by modelGen in future -SD
      UmlMetamodel.fix_project_elements!(project)

      all_packages = project.packages(:recurse => true, :include_imports => true)
      all_classifiers = all_packages.collect{|p| p.classifiers }.flatten
      all_classes = all_packages.collect{|p| p.classes }.flatten
      # Determine root classes (those with a <<Root>> stereotype applied)
      root_classes = all_classes.select{|c| c.applied_stereotypes.any?{|s| s.name =~ /^root$/i} }
      # Find all roots (children and implementors of root classes)
      all_roots = root_classes + root_classes.collect{|c| c.all_children}.flatten
      # Determine unique, concrete root classes
      all_concrete_roots = all_roots.reject{|r| r.is_abstract}.uniq
      
      # Define JSON schema
      json_schema = JsonSchemaMetamodel::Schema.new
      
      # Set either oneOf or $ref, depending on number of root classes
      raise "No concrete roots were found. Please apply a <<root>> stereotype to a class to define it as a root." if all_concrete_roots.empty?
      set_schema_definitions(json_schema, all_concrete_roots)
      
      # Set $schema to indicate draft 4 version of JSON Schema
      json_schema.schema = 'http://json-schema.org/draft-04/schema#'
      
      # TODO: This is disabled since it causes validation failures. Figure out what is wrong here
      # Set $id to appropriate value
      #json_schema.id = "http://www.prometheuscomputing.com/#{root_package.name}.json"
      
      json_schema.definitions = build_definitions(all_classifiers)

      # Return generated JSON Schema
      JSON_Graph_State.json_getter_overrides_by_class = JsonSchemaMetamodel::JSON_SERIALIZATION_SCHEME
      JSON_Graph_State.no_references!
      JSON_Graph_State.no_classNames!
      JSON_Graph.omit_nil_attributes = true
      options = {:max_nesting => 1000}
      json_schema_string = JSON_Graph.pretty_generate(json_schema, options)
    end

    # Build a hash of definitions of all classifiers
    def build_definitions(all_classifiers)
      # Definitions are type: object, which translates to a Hash in Ruby
      definitions = {}
      all_classifiers.sort_by{|c| UmmJsonSchema.json_class_name(c)}.each do |classifier|
        # Skip abstract classifiers except for abstract interfaces
        next if !classifier.interface? && classifier.is_abstract
        # Skip classifiers located in external schemas
        from_external_schema = classifier.containers.any? do |p| 
          p.applied_stereotypes.any?{|s| s.name == 'JSON Schema' && s.applied_tags.any?{|t| t.name == 'external_schema'}}
        end
        next if from_external_schema
        definitions[UmmJsonSchema.json_class_name(classifier)] = build_definition(classifier)
      end
      definitions
    end

    # Build a definition schema for a classifier
    def build_definition(classifier)
      defn = JsonSchemaMetamodel::Schema.new

      if classifier.documentation
        defn.description = fix_quotes_strip_html_and_whitespace(classifier.documentation)
      end

      if classifier.enumeration?
        defn.type = 'string'
        defn.enum = classifier.all_literals.collect{|literal| literal.name}

        defn.description = defn.description ? defn.description + "\n" : ""
        defn.description += "Values:\n"
        defn.description += classifier.all_literals.map{|literal| "\t" + literal.name +
          (literal.documentation ?
             ": " + fix_quotes_strip_html_and_whitespace(literal.documentation) : "")}.join("\n")
      elsif classifier.primitive?
        type_info = UmmJsonSchema.uml_json_type_mapping[classifier.base_primitive]
        if type_info
          defn.type = type_info[:type]
          defn.format = type_info[:format] if type_info[:format]
        else
           # No type mapping is available. Print warning, and use string type
          puts "Warning: No type mapping for primitive '#{classifier.qualified_name}' with base primitive type: #{classifier.base_primitive}. Assuming string type."
          defn.type = 'string'
        end
        apply_primitive_restrictions!(defn, classifier)
      elsif classifier.interface?
        interfaces = [classifier] + classifier.all_children
        implementors = interfaces.collect{|i| i.implementors}.flatten
        all_implementors = implementors + implementors.collect{|i| i.all_children}.flatten
        all_concrete_implementors = all_implementors.reject{|c| c.is_abstract}.uniq
        # Set either oneOf or $ref, depending on number of implementors
        set_schema_definitions(defn, all_concrete_implementors)
      else # classifier is a non-primitive, non-enumeration
        # Build properties hash for the definition
        properties = {}
        required_properties = []
        # Whether or not this classifier must be associated by reference to another classifier
        is_referenced = classifier.all_properties.any?{|p| p.association? && p.opposite && p.opposite.is_navigable && !p.opposite.composite?}
        
        # Add '@id' property definition if this classifier is referenced (and thus needs an @id)
        if is_referenced
          id_name = '@id'
          id_defn = JsonSchemaMetamodel::Schema.new
          id_defn.type = 'string'
          properties[id_name] = id_defn
          required_properties << id_name
        end
        
        # Add '@type' specification property
        # This property provides unambiguous certainty of which class definition is being used by JSON instances.
        # TODO: update JSON binding to allow specification of types, so that types without children do not need
        #       to have @type specified.
        type_name = '@type'
        type_defn = JsonSchemaMetamodel::Schema.new
        type_defn.type = 'string'
        type_defn.enum = ["#{UmmJsonSchema.json_class_name(classifier)}"]
        properties[type_name] = type_defn
        required_properties << type_name
        
        # Warn about overrides
        # First get all properties, including those that are overridden
        classifiers = [classifier] + classifier.all_ancestors
        all_ancestor_properties = classifiers.collect{|c| c.properties }.flatten
        # Now check for duplicate names
        dups = all_ancestor_properties.group_by{|p| build_property_name(p) }.select{|name, properties| properties.count > 1}
        dups.each do |name, properties|
          puts "Warning: #{classifier.name} has overridden property: #{name}"
          properties.each do |p|
            puts "           Found #{name} definition on #{p.owner.name}"
          end
        end
        
        # Define definition properties from class properties
        property_defns = classifier.all_properties.collect do |property|
          next unless property.is_navigable
          property_name, property_schema = build_property_schema(property)
          # properties[property_name] = property_schema
          is_required = property.lower > 0
          required_properties << property_name if is_required
          [property_name, property_schema]
        end.compact
        
        # Add defined properties to properties hash, in order determined by property_name
        # This is done so that the output to JSON will appear alphabetically, since most Ruby
        # implementations of Hash will output keys in order of insertion.
        property_defns.sort_by{|e| e[0]}.each do |property_name, property_schema|
          properties[property_name] = property_schema
        end
        
        # Set definition properties to constructed hash
        defn.properties = properties
        # Set definition type to 'object'
        defn.type = 'object'
        # No additional properties beyond those defined are allowed
        defn.additionalProperties = false
        # Set required properties (if any)
        defn.required = required_properties.sort if required_properties.any?
      end
      
      defn
    end
    
    # Apply any primitive restrictions to the passed definition expressed via properties on the primitive classifier
    def apply_primitive_restrictions!(defn, classifier)
      # Iterate over primitive properties, interpreting them as restrictions on the primitive value
      classifier.all_properties.each do |property|
        # Ignore any properties that do not specify a restriction value
        next unless restriction_value = property.default_value
        
        # Determine whether this a valid restriction on the property and appropriately convert supplied restriction value
        # Note that if 'format' is specified as a restriction, it will override one that may have been specified by the uml_json_type_mapping type_info.
        valid_restriction = case
        when defn.type == 'string' && (property.name == 'format' || property.name == 'pattern')
          restriction_value = restriction_value.to_s
          true
        when defn.type == 'string' && (property.name == 'minLength' || property.name == 'maxLength')
          # FIXME: Should replace this with a solution that does not resort to rescue
          restriction_value = Integer(restriction_value) rescue nil
          true
        when (defn.type == 'number' || defn.type == 'integer') && (property.name == 'minimum' || property.name == 'maximum' || property.name == 'multipleOf')
          # FIXME: Should replace this with a solution that does not resort to rescue
          restriction_value = Integer(input) rescue Float(input) rescue nil
          true
        when (defn.type == 'number' || defn.type == 'integer') && (property.name == 'exclusiveMinimum' || property.name == 'exclusiveMaximum')
          restriction_value = convert_to_boolean(restriction_value)
          true
        else
          false
        end
        
        # Apply the restriction to the definition if it is valid and a valid value was supplied
        if valid_restriction && restriction_value
          defn.send(property.name + '=', restriction_value)
        else
          puts "Warning: Ignoring invalid JSON restriction '#{property.name}' or restriction value '#{restriction_value}' specified for primitive: #{classifier.qualified_name}."
        end
      end
    end
    
    # Return a boolean true/false based on input value or nil to indicate an invalid value
    # Essentially the same as Ruby true/false calculation with special handling for string true/false values
    def convert_to_boolean(value)
      if value.is_a?(String)
        return false if value =~ /^false$/i
        return true if value =~ /^true$/i
        nil
      else
        !!value
      end
    end
    
    def build_property_name(property)
      name = property.name
      name ||= property.type.name
      name
    end
    
    # Build a name and definition schema for a property
    def build_property_schema(property)
      name = build_property_name(property)
      prop = JsonSchemaMetamodel::Schema.new
      if property.type.is_a?(UmlMetamodel::Primitive) && type_info = UmmJsonSchema.uml_json_type_mapping[property.type]
        # type_info = UmmJsonSchema.uml_json_type_mapping[property.type]
        if type_info
          prop.type = type_info[:type]
          prop.format = type_info[:format] if type_info[:format]
        else
          # No type mapping is available. Print warning, and use string type
          puts "Warning: No type mapping for property #{property.qualified_name} with type: #{property.type ? property.type.qualified_name : 'nil'}"
          prop.type = 'string'
        end
      elsif property.type # Has a type, but is not a primitive
        if property.type.qualified_name == "Gui_Builder_Profile::RichText"
          prop.type = 'string'
        else
          classifier = property.type
          all_classifiers = [classifier] + classifier.all_children
          # Exclude any abstract classes to determine all concrete classes
          all_concrete_classifiers = all_classifiers.reject{|c| c.is_abstract}.uniq
          if !(property.composite?) && !(property.attribute?)
            # Build a reference property by modifying name and prop
            name, prop = build_reference!(property, name, prop, all_concrete_classifiers)
          else
            # Set either oneOf or $ref, depending on number of concrete classifiers
            set_schema_definitions(prop, all_concrete_classifiers)
          end
        end
      end
      # If property has a multiplicity > 1, wrap property in array schema
      prop = wrap_in_array_schema(property, prop) if property.multiple?
      # Return name and property schema
      [name, prop]
    end
  
    # Modify the passed name, schema to make a reference property
    def build_reference!(property, name, schema, ref_types)
      ref_types = ref_types.collect{|c| UmmJsonSchema.json_class_name(c)}
      schema.additional_attributes[:refTypes] = ref_types
      case options[:references]
      when :id_attributes
        name += property.multiple? ? options[:id_suffix_plural] : options[:id_suffix]
        schema.type = 'string'
      when :ref_objects
        schema.type = 'object'
        ref = JsonSchemaMetamodel::Schema.new
        ref.type = 'string'
        schema.additionalProperties = false
        schema.properties = {'@ref' => ref }
        schema.required = ['@ref']
      else
        raise "Invalid references mode supplied: #{options[:references]}"
      end
      [name, schema]
    end
    
    # Set either oneOf or $ref to the definitions for the classifiers, depending on number of passed classifiers
    def set_schema_definitions(schema, classifiers)
      if classifiers.count > 1
        schema.oneOf = definitions_for(classifiers)
      elsif classifiers.count == 1
        schema.ref = definition_for(classifiers.first)
      end
    end
    
    # Return an array of schemas referencing the passed classifier's definitions
    def definitions_for(classifiers)
      classifiers.collect{|c|
        ref_schema = JsonSchemaMetamodel::Schema.new
        ref_schema.ref = definition_for(c)
        ref_schema
      }
    end
    
    # Return a string referencing the passed classifier's definition
    def definition_for(classifier)
      external_schema = nil
      classifier.containers.each do |p| 
        json_schema = p.applied_stereotypes.find{|s| s.name == 'JSON Schema'}
        external_schema_tag = json_schema.applied_tags.find{|t| t.name == 'external_schema'} if json_schema
        external_schema = external_schema_tag.value if external_schema_tag
        break if external_schema
      end
      "#{external_schema}\#/definitions/#{UmmJsonSchema.json_class_name(classifier)}"
    end
    
    def wrap_in_array_schema(property, schema)
      array_schema = JsonSchemaMetamodel::Schema.new
      array_schema.type = 'array'
      # Set lower and upper bounds
      array_schema.minItems = property.lower
      array_schema.maxItems = property.upper unless property.upper == Float::INFINITY
      # Set wrapped schema as 'items' matcher
      array_schema.items = schema
      # Return array schema
      array_schema
    end

    # Handle badly formatted MagicDraw HTML documentation, stripping it of HTML tags and excessive whitespace
    def fix_quotes_strip_html_and_whitespace(text)
      # Replace non breaking spaces with regular space
      text.gsub!(/(&nbsp;|&#160;)/, ' ')
      # Fix quotes
      text.gsub!(/&rsquo;/, '\'')
      text.gsub!(/&lsquo;/, '\'')
      text.gsub!(/&rdquo;/, '"')
      text.gsub!(/&ldquo;/, '"')
      # Remove HTML tags
      text.gsub!(/<[^>]*>/, '')
      # Remove leading and trailing whitespace for entire string
      text.strip!
      # Remove trailing whitespace for individual lines
      text.gsub!(/[ \t]+$/, '')
      # Remove multiple sequential empty lines
      lines = text.split("\n")
      lines.each_with_index{|line, i| lines[i] = nil if (lines[i].empty? && lines[i+1].empty?)}
      lines.compact!
      text = lines.join("\n")
    end
  end
end
