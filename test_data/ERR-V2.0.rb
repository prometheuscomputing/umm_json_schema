project "NIST V2.0 - ElectionResultsReporting", :original_filename => "NIST V2.0 - ElectionResultsReporting.xml", :generated_by => "plugin.modelGen-2.1.1", :generated_at => "2018-04-13 16:38:28 UTC", :additional_info => {:selected_model=>{:name=>"Data", :id=>"eee_1045467100313_135436_1", :pluralize_role_names=>false, :snakecase_role_names=>false}}, :id => "_18_0_2_6340208_1523637467106_151324_4543" do
  package "ElectionResults", :id => "_17_0_2_4_78e0236_1389366160416_780968_2277" do
    applied_stereotype :instance_of => "XML Schema::XML Schema" do
      applied_tag :instance_of => "XML Schema::XML Schema::targetNamespace", :value => "NIST_V<MAJOR_VERSION>_election_results_reporting.xsd"
      applied_tag :instance_of => "XML Schema::XML Schema::prefix", :value => "er"
      applied_tag :instance_of => "XML Schema::XML Schema::only_keep_compositions", :value => true
      applied_tag :instance_of => "XML Schema::XML Schema::always_keep_compositions", :value => true
      applied_tag :instance_of => "XML Schema::XML Schema::object_id_only_if_referenced", :value => true
      applied_tag :instance_of => "XML Schema::XML Schema::append_id_to_reference_names", :value => true
      applied_tag :instance_of => "XML Schema::XML Schema::id_attribute_name", :value => "ObjectId"
      applied_tag :instance_of => "XML Schema::XML Schema::id_suffix_for_references", :value => "Id"
      applied_tag :instance_of => "XML Schema::XML Schema::full_version", :value => "2.0"
      applied_tag :instance_of => "XML Schema::XML Schema::schema_filename", :value => "NIST_V<MAJOR_VERSION>_election_results_reporting.xsd"
      applied_tag :instance_of => "XML Schema::XML Schema::major_version", :value => "2"
      applied_tag :instance_of => "XML Schema::XML Schema::use_idrefs", :value => true
      applied_tag :instance_of => "XML Schema::XML Schema::id_suffix_for_plural_references", :value => "Ids"
      applied_tag :instance_of => "XML Schema::XML Schema::generate_attributes_as_elements", :value => true
      applied_tag :instance_of => "XML Schema::XML Schema::sort_other_attributes_following_base_attributes", :value => true
      applied_tag :instance_of => "XML Schema::XML Schema::sort_end_attributes_following_start_attributes", :value => true
      applied_tag :instance_of => "XML Schema::XML Schema::beginning_comment", :value => "Version 2.0, NIST Election Results CDF Specification, National Institute of Standards and Technology"
    end
    association :properties => ["_17_0_2_4_f71035d_1436214223050_585224_2515", "_17_0_2_4_f71035d_1436214223050_896129_2516"], :id => "_17_0_2_4_f71035d_1436214223049_930963_2514"
    association :properties => ["_17_0_2_4_f71035d_1426189065873_416235_2489", "_17_0_2_4_f71035d_1426189065874_694421_2490"], :id => "_17_0_2_4_f71035d_1426189065873_453196_2488"
    association :properties => ["ElectionResults::BallotStyle::Party", "_18_0_2_6340208_1427483833143_798813_4566"], :id => "_18_0_2_6340208_1427483833143_440325_4564", :documentation => "This association is populated if the BallotStyle is specific to a particular party or parties"
    association :properties => ["_18_0_2_6340208_1498659302196_151943_4608", "_18_0_2_6340208_1498659302197_511384_4609"], :id => "_18_0_2_6340208_1498659302196_521744_4607"
    association :properties => ["ElectionResults::Candidate::Party", "_17_0_2_4_78e0236_1389366597377_496785_2699"], :id => "_17_0_2_4_78e0236_1389366597377_866539_2697"
    association :properties => ["ElectionResults::Candidate::Person", "_17_0_5_1_43401a7_1400624143347_340058_3605"], :id => "_17_0_5_1_43401a7_1400624143347_368658_3603"
    association :properties => ["ElectionResults::CandidateContest::Office", "ElectionResults::Office::Contest"], :id => "_17_0_5_1_43401a7_1400624734486_670053_3698"
    association :properties => ["ElectionResults::CandidateContest::PrimaryParty", "_17_0_2_4_78e0236_1389735000217_903057_4017"], :id => "_17_0_2_4_78e0236_1389735000217_463954_4015"
    association :properties => ["ElectionResults::CandidateSelection::Candidate", "_17_0_2_4_d420315_1392145686219_248226_2595"], :id => "_17_0_2_4_d420315_1392145686218_819551_2593"
    association :properties => ["ElectionResults::CandidateSelection::EndorsementParty", "_17_0_2_4_d420315_1391370669921_979852_2560"], :id => "_17_0_2_4_d420315_1391370669921_833502_2558"
    association :properties => ["ElectionResults::Coalition::Contest", "_18_0_2_6340208_1427484451489_40300_4610"], :id => "_18_0_2_6340208_1427484451489_554894_4608", :documentation => "If a Coalition is only valid for certain contest(s), they are specified via this association.\nIf no contests are specified, it is assumed that the coalition is valid for all contests."
    association :properties => ["ElectionResults::Coalition::Party", "_18_0_2_6340208_1425647321122_252928_4745"], :id => "_18_0_2_6340208_1425647321121_751675_4743"
    association :properties => ["_17_0_2_4_f71035d_1443105009955_640511_2261", "_17_0_2_4_f71035d_1443105009955_327305_2262"], :id => "_17_0_2_4_f71035d_1443105009955_560558_2260"
    association :properties => ["_17_0_2_4_f71035d_1429176643252_845913_2230", "_17_0_2_4_f71035d_1429176643252_169668_2231"], :id => "_17_0_2_4_f71035d_1429176643251_135656_2229"
    association :properties => ["_17_0_2_4_78e0236_1389366541302_23458_2637", "_17_0_2_4_78e0236_1389366541302_32185_2638"], :id => "_17_0_2_4_78e0236_1389366541302_20872_2636"
    association :properties => ["_17_0_2_4_78e0236_1397059165386_471037_2443", "_17_0_2_4_78e0236_1397059165387_750452_2444"], :id => "_17_0_2_4_78e0236_1397059165386_325090_2442"
    association :properties => ["ElectionResults::Counts::GpUnit", "_17_0_2_4_78e0236_1389372033343_628513_2883"], :id => "_17_0_2_4_78e0236_1389372033343_102129_2881"
    association :properties => ["_18_0_2_6340208_1508176491404_114747_4598", "_18_0_2_6340208_1508176491404_255059_4599"], :id => "_18_0_2_6340208_1508176491404_757932_4597"
    association :properties => ["_17_0_2_4_f71035d_1426789041442_265842_2746", "_17_0_2_4_f71035d_1426789041442_477102_2747"], :id => "_17_0_2_4_f71035d_1426789041442_310913_2745"
    association :properties => ["_17_0_2_4_f71035d_1426788786008_599642_2643", "_17_0_2_4_f71035d_1426788786008_623450_2644"], :id => "_17_0_2_4_f71035d_1426788786008_813169_2642"
    association :properties => ["_17_0_2_4_f71035d_1426788714136_545781_2616", "_17_0_2_4_f71035d_1426788714136_149681_2617"], :id => "_17_0_2_4_f71035d_1426788714136_242693_2615"
    association :properties => ["ElectionResults::Election::ContactInformation", "_18_0_2_6340208_1429710511392_725818_4546"], :id => "_18_0_2_6340208_1429710511392_129137_4544"
    association :properties => ["ElectionResults::Election::ElectionScope", "_17_0_2_4_f71035d_1426102211616_590886_2332"], :id => "_17_0_2_4_f71035d_1426102211616_329326_2330"
    association :properties => ["_18_0_2_6340208_1441312459706_787590_4464", "_18_0_2_6340208_1441312459706_208117_4465"], :id => "_18_0_2_6340208_1441312459706_896965_4463"
    association :properties => ["ElectionResults::ElectionAdministration::ElectionOfficialPerson", "_18_0_2_6340208_1441312523524_39228_4514"], :id => "_18_0_2_6340208_1441312523523_851585_4512"
    association :properties => ["_17_0_2_4_f71035d_1426102320351_976615_2363", "_17_0_2_4_f71035d_1426102320352_797542_2364"], :id => "_17_0_2_4_f71035d_1426102320351_400974_2362"
    association :properties => ["_17_0_2_4_f71035d_1426788982595_725441_2719", "_17_0_2_4_f71035d_1426788982595_803482_2720"], :id => "_17_0_2_4_f71035d_1426788982595_479092_2718"
    association :properties => ["_17_0_2_4_f71035d_1426788177421_963220_2552", "_17_0_2_4_f71035d_1426788177421_211291_2553"], :id => "_17_0_2_4_f71035d_1426788177420_185066_2551"
    association :properties => ["_17_0_2_4_f71035d_1433183761792_828366_2293", "_17_0_2_4_f71035d_1433183761792_285048_2294"], :id => "_17_0_2_4_f71035d_1433183761792_160023_2292"
    association :properties => ["_17_0_2_4_f71035d_1426788475880_621446_2579", "_17_0_2_4_f71035d_1426788475880_755867_2580"], :id => "_17_0_2_4_f71035d_1426788475880_502298_2578"
    association :properties => ["ElectionResults::ElectionReport::Person", "_17_0_2_4_f71035d_1426788901070_60377_2693"], :id => "_17_0_2_4_f71035d_1426788901070_922999_2691"
    association :properties => ["ElectionResults::GpUnit::ComposingGpUnit", "_17_0_2_4_f71035d_1442233726834_146073_2239"], :id => "_17_0_2_4_f71035d_1442233726833_662931_2237"
    association :properties => ["ElectionResults::InternationalizedText::Text", "_17_0_2_4_f71035d_1428953680102_554788_2233"], :id => "_17_0_2_4_f71035d_1428953680099_569555_2224"
    association :properties => ["_17_0_2_4_f71035d_1428489163406_781378_2243", "_17_0_2_4_f71035d_1428489163407_121972_2244"], :id => "_17_0_2_4_f71035d_1428489163406_37606_2242"
    association :properties => ["ElectionResults::Office::ContactInformation", "_17_0_2_4_f71035d_1429207941244_754871_2263"], :id => "_17_0_2_4_f71035d_1429207941244_231047_2261"
    association :properties => ["ElectionResults::Office::ElectoralDistrict", "_17_0_5_1_43401a7_1400701616170_761517_3685"], :id => "_17_0_5_1_43401a7_1400701616170_41382_3683"
    association :properties => ["ElectionResults::Office::OfficeHolderPerson", "_17_0_2_4_f71035d_1429177738058_372465_2295"], :id => "_17_0_2_4_f71035d_1429177738058_446809_2293"
    association :properties => ["_17_0_2_4_f71035d_1433183725966_939706_2266", "_17_0_2_4_f71035d_1433183725966_600298_2267"], :id => "_17_0_2_4_f71035d_1433183725966_629725_2265"
    association :properties => ["ElectionResults::OfficeGroup::SubOfficeGroup", "_17_0_2_4_f71035d_1433429126207_758556_2536"], :id => "_17_0_2_4_f71035d_1433429126206_530492_2534"
    association :properties => ["ElectionResults::OrderedContest::Contest", "_17_0_3_43401a7_1394477833536_208526_3250"], :id => "_17_0_3_43401a7_1394477833535_717588_3248"
    association :properties => ["ElectionResults::OrderedContest::OrderedBallotSelection", "_17_0_3_43401a7_1394477871278_177175_3271"], :id => "_17_0_3_43401a7_1394477871277_991242_3269"
    association :properties => ["_18_0_2_6340208_1508176572777_602711_4625", "_18_0_2_6340208_1508176572777_556284_4626"], :id => "_18_0_2_6340208_1508176572777_170116_4624"
    association :properties => ["ElectionResults::Party::LeaderPerson", "ElectionResults::Person::LeaderOfParty"], :id => "_18_0_2_6340208_1506626085733_10844_4569"
    association :properties => ["ElectionResults::PartyRegistration::Party", "_17_0_2_4_78e0236_1394566867126_548112_2852"], :id => "_17_0_2_4_78e0236_1394566867126_26417_2850"
    association :properties => ["ElectionResults::PartySelection::Party", "_17_0_2_4_f71035d_1426520590194_322005_2565"], :id => "_17_0_2_4_f71035d_1426520590194_477054_2563"
    association :properties => ["_17_0_2_4_f71035d_1409158807179_27867_2211", "_17_0_2_4_f71035d_1409158807179_298532_2212"], :id => "_17_0_2_4_f71035d_1409158807179_380963_2210"
    association :properties => ["ElectionResults::Person::Party", "_17_0_5_1_43401a7_1400673254137_213306_3703"], :id => "_17_0_5_1_43401a7_1400673254137_821852_3701"
    association :properties => ["_17_0_2_4_78e0236_1389366667508_288382_2752", "ElectionResults::Contest::ElectoralDistrict"], :id => "_17_0_2_4_78e0236_1389366667508_159320_2751", :documentation => "This class is used to identify and report on information regarding various types of localities, including jurisdictions and districts down to precincts and ultimately devices."
    association :properties => ["_18_0_2_6340208_1441312768897_951923_4552", "_18_0_2_6340208_1441312768897_19641_4553"], :id => "_18_0_2_6340208_1441312768897_831182_4551"
    association :properties => ["_17_0_2_4_f71035d_1409141976968_835708_2555", "_17_0_2_4_f71035d_1409141976969_821908_2556"], :id => "_17_0_2_4_f71035d_1409141976968_40331_2554"
    association :properties => ["_17_0_2_4_f71035d_1426084480956_43890_2738", "_17_0_2_4_f71035d_1426084480956_314639_2739"], :id => "_17_0_2_4_f71035d_1426084480956_788376_2737"
    association :properties => ["ElectionResults::ReportingUnit::Authority", "_17_0_5_1_43401a7_1400624362436_508341_3663"], :id => "_17_0_5_1_43401a7_1400624362436_479294_3661"
    association :properties => ["ElectionResults::ReportingUnit::ContactInformation", "_17_0_2_4_f71035d_1429207854278_914805_2232"], :id => "_17_0_2_4_f71035d_1429207854277_122914_2230"
    association :properties => ["ElectionResults::RetentionContest::Candidate", "_18_0_2_6340208_1425646466278_808955_4617"], :id => "_18_0_2_6340208_1425646466278_466411_4615"
    association :properties => ["ElectionResults::RetentionContest::Office", "_18_0_2_6340208_1425646257225_938757_4581"], :id => "_18_0_2_6340208_1425646257224_297476_4579"
    association :properties => ["_18_0_2_6340208_1427122219893_697937_4628", "_18_0_2_6340208_1427122219893_765770_4629"], :id => "_18_0_2_6340208_1427122219893_86491_4627"
    association :properties => ["_17_0_2_4_f71035d_1409080509711_839030_2261", "_17_0_2_4_f71035d_1409080509711_420107_2262"], :id => "_17_0_2_4_f71035d_1409080509711_376317_2260"
    association :properties => ["_17_0_2_4_78e0236_1389372026000_739694_2861", "_17_0_2_4_78e0236_1389372026000_187007_2862"], :id => "_17_0_2_4_78e0236_1389372026000_31111_2860"
    klass "AnnotatedString", :id => "_18_0_2_6340208_1497553224568_429892_4565" do
      property "Annotation", :type => "ElectionResults::ShortString", :id => "_18_0_2_6340208_1497553268113_377804_4591"
      property "Content", :type => "Primitives (Class based)::xsd::string", :lower => 1, :id => "_18_0_2_6340208_1497553239224_304629_4586" do
        applied_stereotype :instance_of => "XML Schema::simpleContent"
      end
    end
    klass "AnnotatedUri", :id => "_18_0_2_6340208_1498658436378_308208_4565" do
      property "Annotation", :type => "ElectionResults::ShortString", :id => "_18_0_2_6340208_1498658485557_944083_4589"
      property "Content", :type => "Primitives (Class based)::xsd::anyURI", :lower => 1, :id => "_18_0_2_6340208_1498658457673_356828_4586" do
        applied_stereotype :instance_of => "XML Schema::simpleContent"
      end
    end
    klass "BallotCounts", :parents => ["ElectionResults::Counts"], :id => "_17_0_2_4_78e0236_1397156576157_466818_2461" do
      property "BallotsCast", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_f71035d_1426507405852_561323_2207"
      property "BallotsOutstanding", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_f71035d_1426007069403_906238_2480"
      property "BallotsRejected", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_f71035d_1430935326894_744193_2504"
      property nil, :type => "ElectionResults::Election", :is_navigable => false, :id => "_18_0_2_6340208_1508176491404_255059_4599"
    end
    klass "BallotMeasureContest", :parents => ["ElectionResults::Contest"], :id => "_17_0_2_4_78e0236_1389366932057_929676_2783", :documentation => "Class/element for type of contest involving ballot measures (i.e.,referenda). Inherits attributes from the Contest class/element; includes other attributes for including text from the ballot measure if available." do
      property "ConStatement", :type => "ElectionResults::InternationalizedText", :id => "_17_0_2_4_f71035d_1426519421663_178011_2491"
      property "EffectOfAbstain", :type => "ElectionResults::InternationalizedText", :id => "_17_0_2_4_f71035d_1426519513798_649615_2501"
      property "FullText", :type => "ElectionResults::InternationalizedText", :id => "_17_0_2_4_78e0236_1389733794199_310242_3824", :documentation => "The full text of the referendum."
      property "InfoUri", :type => "Primitives (Class based)::xsd::anyURI", :id => "_17_0_2_4_f71035d_1441214816702_348487_2511"
      property "OtherType", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1426550214099_344315_2520"
      property "PassageThreshold", :type => "ElectionResults::InternationalizedText", :id => "_17_0_2_4_f71035d_1426519474233_980390_2497"
      property "ProStatement", :type => "ElectionResults::InternationalizedText", :id => "_17_0_2_4_f71035d_1426519388364_485730_2487"
      property "SummaryText", :type => "ElectionResults::InternationalizedText", :id => "_17_0_2_4_78e0236_1389733722505_364946_3820", :documentation => "The summary text of the referendum."
      property "Type", :type => "ElectionResults::BallotMeasureType", :id => "_17_0_2_4_f71035d_1426550181692_978243_2516"
    end
    klass "BallotMeasureSelection", :parents => ["ElectionResults::BallotSelection"], :id => "_17_0_2_4_78e0236_1389372163799_981952_2926", :documentation => "Class/element that is a type of ballot selection specific to ballot measures, e.g., \"yes\", \"no\"." do
      property "Selection", :type => "ElectionResults::InternationalizedText", :lower => 1, :id => "_17_0_2_4_78e0236_1389710917151_765889_2176"
    end
    klass "BallotSelection", :id => "_17_0_2_4_78e0236_1389372124445_11077_2906", :documentation => "Class/element that serves as a selection for a contest, so that counts can be reported and associated with device types and voting classes. Can be for a ballot measure, candCodeate, overvote, write-in, etc.", :is_abstract => true do
      property nil, :type => "ElectionResults::Contest", :is_navigable => false, :id => "_17_0_2_4_78e0236_1389366541302_32185_2638"
      property nil, :type => "ElectionResults::OrderedContest", :is_navigable => false, :id => "_17_0_3_43401a7_1394477871278_177175_3271"
      property "SequenceOrder", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_f71035d_1426296042287_22607_2200", :documentation => "Defines the sequence in which this BallotSelection appears within its Contest when displaying results."
      property nil, :type => "ElectionResults::VoteCounts", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_78e0236_1389372026000_187007_2862"
    end
    klass "BallotStyle", :id => "_17_0_2_4_78e0236_1389366224561_797289_2360", :documentation => "Class/element for associating a ballot style with a reporting unit, e.g., a precinct." do
      property nil, :type => "ElectionResults::Election", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426789041442_477102_2747"
      property "ExternalIdentifier", :type => "ElectionResults::ExternalIdentifier", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1441377633582_32184_2220"
      property nil, :type => "ElectionResults::GpUnit", :lower => 1, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1436214223050_585224_2515"
      property "ImageUri", :type => "Primitives (Class based)::xsd::anyURI", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1428529376950_608184_2486"
      property nil, :type => "ElectionResults::OrderedContest", :aggregation => :composite, :is_ordered => true, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1426189065873_416235_2489"
      property "Party", :type => "ElectionResults::Party", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1427483833143_782361_4565"
    end
    klass "Candidate", :id => "_17_0_2_4_78e0236_1389366272694_544359_2440", :documentation => "Class/element for describing information about a candidate in a contest." do
      property "BallotName", :type => "ElectionResults::InternationalizedText", :lower => 1, :id => "_17_0_2_4_78e0236_1389710816659_20227_2170", :documentation => "Candidate's name as listed on the ballot."
      property nil, :type => "ElectionResults::CandidateSelection", :is_navigable => false, :id => "_17_0_2_4_d420315_1392145686219_248226_2595"
      property nil, :type => "ElectionResults::ContactInformation", :aggregation => :composite, :id => "_18_0_2_6340208_1498659302196_151943_4608"
      property nil, :type => "ElectionResults::Election", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426788786008_623450_2644"
      property "ExternalIdentifier", :type => "ElectionResults::ExternalIdentifier", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1430405890311_465205_2454"
      property "FileDate", :type => "Primitives (Class based)::xsd::date", :id => "_17_0_2_4_f71035d_1400615133498_375109_2704"
      property "IsIncumbent", :type => "Primitives (Class based)::xsd::boolean", :id => "_17_0_2_4_f71035d_1401280462978_833890_2462"
      property "IsTopTicket", :type => "Primitives (Class based)::xsd::boolean", :id => "_17_0_2_4_f71035d_1403276277476_329066_2190"
      property "Party", :type => "ElectionResults::Party", :id => "_17_0_2_4_78e0236_1389366597377_433664_2698"
      property "Person", :type => "ElectionResults::Person", :id => "_17_0_5_1_43401a7_1400624143347_418542_3604"
      property "PostElectionStatus", :type => "ElectionResults::CandidatePostElectionStatus", :id => "_17_0_2_4_78e0236_1389797778404_982263_4132", :documentation => "Registration status of the candidate, e.g., filed, qualified, etc."
      property "PreElectionStatus", :type => "ElectionResults::CandidatePreElectionStatus", :id => "_17_0_2_4_f71035d_1426535359938_597654_2790"
      property nil, :type => "ElectionResults::RetentionContest", :is_navigable => false, :id => "_18_0_2_6340208_1425646466278_808955_4617"
    end
    klass "CandidateContest", :parents => ["ElectionResults::Contest"], :id => "_17_0_2_4_78e0236_1389366970084_183781_2806", :documentation => "Class/element for type of contest involving one or more candCodeates. Inherits attributes from the Contest class/element; includes other attributes to indicate number of candCodeates that are elected to the office (usually 1) and the number of votes that a voter can cast in the contest (usually 1) so as to support the n-of-m vote variation." do
      property "NumberElected", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_78e0236_1389797739578_12603_4129", :documentation => "Number of candidates that are elected in the contest (\"M\" of N-of-M)."
      property "NumberRunoff", :type => "Primitives (Class based)::xsd::integer", :id => "_18_0_2_6340208_1498659576131_900303_4636"
      property "Office", :type => "ElectionResults::Office", :is_ordered => true, :upper => Float::INFINITY, :id => "_17_0_5_1_43401a7_1400624734486_732685_3699"
      property "PrimaryParty", :type => "ElectionResults::Party", :upper => Float::INFINITY, :id => "_17_0_2_4_78e0236_1389735000217_728769_4016"
      property "VotesAllowed", :type => "Primitives (Class based)::xsd::integer", :lower => 1, :id => "_17_0_2_4_78e0236_1389797728177_241732_4126", :documentation => "Maximum number of votes/writeins per voter in this contest (\"N\" of N-of-M)."
    end
    klass "CandidateSelection", :parents => ["ElectionResults::BallotSelection"], :id => "_17_0_2_4_d420315_1392145640524_831493_2562", :documentation => "Class/element that is a type of ballot selection specific to selecting a candCodeate in a contest for an office." do
      property "Candidate", :type => "ElectionResults::Candidate", :is_ordered => true, :upper => Float::INFINITY, :id => "_17_0_2_4_d420315_1392145686219_781480_2594"
      property "EndorsementParty", :type => "ElectionResults::Party", :upper => Float::INFINITY, :id => "_17_0_2_4_d420315_1391370669921_519404_2559"
      property "IsWriteIn", :type => "Primitives (Class based)::xsd::boolean", :id => "_17_0_2_4_78e0236_1389797859448_230579_4174", :documentation => "Bolean to indicate whether the candidate is a write-in."
    end
    klass "Coalition", :parents => ["ElectionResults::Party"], :id => "_18_0_2_6340208_1425647247631_162984_4712" do
      property "Contest", :type => "ElectionResults::Contest", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1427484451489_775363_4609"
      property "Party", :type => "ElectionResults::Party", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1425647321121_89855_4744"
    end
    klass "ContactInformation", :id => "_17_0_5_1_43401a7_1400624327407_326048_3637" do
      property "AddressLine", :type => "Gui_Builder_Profile::RichText", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1425645912998_115448_4529", :documentation => "A listing of each line in the contact's postal address that follows the addressee's Name. Name is assumed to be the first line of the address."
      property nil, :type => "ElectionResults::Candidate", :is_navigable => false, :id => "_18_0_2_6340208_1498659302197_511384_4609"
      property "Directions", :type => "ElectionResults::InternationalizedText", :id => "_17_0_2_4_f71035d_1443105112875_46223_2290"
      property nil, :type => "ElectionResults::Election", :is_navigable => false, :id => "_18_0_2_6340208_1429710511392_725818_4546"
      property nil, :type => "ElectionResults::ElectionAdministration", :is_navigable => false, :id => "_18_0_2_6340208_1441312459706_208117_4465"
      property "Email", :type => "ElectionResults::AnnotatedString", :upper => Float::INFINITY, :id => "_17_0_5_1_43401a7_1400668036651_743620_3650"
      property "Fax", :type => "ElectionResults::AnnotatedString", :upper => Float::INFINITY, :id => "_17_0_5_1_43401a7_1400668021448_721992_3646"
      property "Label", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1441215163702_951734_2515" do
        applied_stereotype :instance_of => "XML Schema::xmlAttribute"
      end
      property nil, :type => "ElectionResults::LatLng", :aggregation => :composite, :id => "_17_0_2_4_f71035d_1443105009955_640511_2261"
      property "Name", :type => "Gui_Builder_Profile::RichText", :id => "_18_0_2_6340208_1429287939709_269212_4416", :documentation => "The contact's name. This is also the first line of the contact's address."
      property nil, :type => "ElectionResults::Office", :is_navigable => false, :id => "_17_0_2_4_f71035d_1429207941244_754871_2263"
      property nil, :type => "ElectionResults::Person", :is_navigable => false, :id => "_17_0_2_4_f71035d_1409158807179_298532_2212"
      property "Phone", :type => "ElectionResults::AnnotatedString", :upper => Float::INFINITY, :id => "_17_0_5_1_43401a7_1400667951215_637516_3638"
      property nil, :type => "ElectionResults::ReportingUnit", :is_navigable => false, :id => "_17_0_2_4_f71035d_1429207854278_914805_2232"
      property nil, :type => "ElectionResults::Schedule", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1429176643252_845913_2230"
      property "Uri", :type => "ElectionResults::AnnotatedUri", :upper => Float::INFINITY, :id => "_17_0_5_1_43401a7_1400668251889_705688_3666"
    end
    klass "Contest", :id => "_17_0_2_4_78e0236_1389366251994_876831_2400", :documentation => "Abstract class /element for a contest. Classes/elements for specific types of contests inherit the attributes and define their own.", :is_abstract => true do
      property "Abbreviation", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_5_1_43401a7_1395831571231_804795_3632"
      property nil, :type => "ElectionResults::BallotSelection", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_78e0236_1389366541302_23458_2637"
      property "BallotSubTitle", :type => "ElectionResults::InternationalizedText", :id => "_17_0_2_4_f71035d_1426519324658_821208_2483"
      property "BallotTitle", :type => "ElectionResults::InternationalizedText", :id => "_17_0_2_4_f71035d_1426519300284_211849_2479"
      property nil, :type => "ElectionResults::Coalition", :is_navigable => false, :id => "_18_0_2_6340208_1427484451489_40300_4610"
      property "CountStatus", :type => "ElectionResults::CountStatus", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1430428675044_368644_2247"
      property nil, :type => "ElectionResults::Election", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426788714136_149681_2617"
      property "ElectoralDistrict", :type => "ElectionResults::ReportingUnit", :lower => 1, :id => "_17_0_2_4_78e0236_1389366667508_703141_2753", :documentation => "For associating a contest with its corresponding district, e.g., a ReportingUnit class/element of type=district, thereby identifying the scope of the contest."
      property "ExternalIdentifier", :type => "ElectionResults::ExternalIdentifier", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1430412090176_814999_2249"
      property "HasRotation", :type => "Primitives (Class based)::xsd::boolean", :id => "_17_0_2_4_f71035d_1426006769365_710376_2474"
      property "Name", :type => "Gui_Builder_Profile::RichText", :lower => 1, :id => "_17_0_2_4_78e0236_1389712460582_306281_2220", :documentation => "Title or name of the contest, e.g., \"Governor\" or \"Question on Legalization of Gambling\"."
      property nil, :type => "ElectionResults::OrderedContest", :is_navigable => false, :id => "_17_0_3_43401a7_1394477833536_208526_3250"
      property nil, :type => "ElectionResults::OtherCounts", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_78e0236_1397059165386_471037_2443"
      property "OtherVoteVariation", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1426537329540_929122_2797"
      property "SequenceOrder", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_f71035d_1426083547931_912709_2690", :documentation => "Defines the sequence in which this Contest appears when displaying results."
      property "SubUnitsReported", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_d420315_1393508523463_695325_3041", :documentation => "Number of precincts that have completed reporting votes for this contest."
      property "TotalSubUnits", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_d420315_1393508532825_910334_3045", :documentation => "Total number of precincts that have this contest on the ballot."
      property "VoteVariation", :type => "ElectionResults::VoteVariation", :id => "_17_0_2_4_78e0236_1389798198604_276106_4268", :documentation => "Vote variation associated with the contest, e.g., n-of-m, etc."
    end
    klass "CountStatus", :id => "_17_0_2_4_f71035d_1430412663878_61362_2269" do
      property "OtherType", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1426077858771_890955_2661"
      property "Status", :type => "ElectionResults::CountItemStatus", :lower => 1, :id => "_17_0_2_4_f71035d_1426077427867_28021_2619"
      property "Type", :type => "ElectionResults::CountItemType", :lower => 1, :id => "_17_0_2_4_f71035d_1426077318387_348887_2615"
    end
    klass "Counts", :id => "_17_0_2_4_78e0236_1389367291663_284973_2835", :documentation => "class/element for reporting on contest vote counts. Contains attributes to  categorize the counts according to  voting classification  (e.g., election day, early voting, etc.) and type of device on which the votes  were cast(e.g., DRE, accessible device, etc.)", :is_abstract => true do
      property "DeviceClass", :type => "ElectionResults::DeviceClass", :id => "_17_0_2_4_f71035d_1430428463182_799732_2239"
      property "GpUnit", :type => "ElectionResults::GpUnit", :lower => 1, :id => "_17_0_2_4_78e0236_1389372033343_84394_2882"
      property "IsSuppressedForPrivacy", :type => "Primitives (Class based)::xsd::boolean", :id => "_17_0_2_4_f71035d_1443037119396_390572_2220"
      property "OtherType", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1426077947627_227957_2665"
      property "Round", :type => "Primitives (Class based)::xsd::integer", :id => "_18_0_5_43401a7_1508329922419_951254_4303"
      property "Type", :type => "ElectionResults::CountItemType", :lower => 1, :id => "_17_0_2_4_f71035d_1401285906925_720136_2261"
    end
    klass "DeviceClass", :id => "_18_0_2_6340208_1425911626288_420556_4530" do
      property "Manufacturer", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1401286171326_648907_2273"
      property "Model", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1401286117587_806540_2269"
      property "OtherType", :type => "Primitives (Class based)::xsd::string", :id => "_18_0_2_6340208_1497894619958_710016_4605"
      property "Type", :type => "ElectionResults::DeviceType", :id => "_17_0_2_4_f71035d_1401285959630_42686_2265"
    end
    klass "Election", :id => "_17_0_2_4_f71035d_1426101822599_430942_2209" do
      property nil, :type => "ElectionResults::BallotCounts", :aggregation => :composite, :upper => Float::INFINITY, :id => "_18_0_2_6340208_1508176491404_114747_4598"
      property nil, :type => "ElectionResults::BallotStyle", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1426789041442_265842_2746"
      property nil, :type => "ElectionResults::Candidate", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1426788786008_599642_2643"
      property "ContactInformation", :type => "ElectionResults::ContactInformation", :aggregation => :composite, :id => "_18_0_2_6340208_1429710511392_588063_4545"
      property nil, :type => "ElectionResults::Contest", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1426788714136_545781_2616"
      property "CountStatus", :type => "ElectionResults::CountStatus", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1430428731982_612772_2251"
      property nil, :type => "ElectionResults::ElectionReport", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426102320352_797542_2364"
      property "ElectionScope", :type => "ElectionResults::ReportingUnit", :lower => 1, :id => "_17_0_2_4_f71035d_1426102211616_609900_2331"
      property "EndDate", :type => "Primitives (Class based)::xsd::date", :lower => 1, :id => "_17_0_2_4_f71035d_1431009646277_24904_2233"
      property "ExternalIdentifier", :type => "ElectionResults::ExternalIdentifier", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1430411992333_911417_2240"
      property "Name", :type => "ElectionResults::InternationalizedText", :lower => 1, :id => "_17_0_2_4_f71035d_1426101865703_367602_2232"
      property "OtherType", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1447709724802_42785_2220"
      property "StartDate", :type => "Primitives (Class based)::xsd::date", :lower => 1, :id => "_17_0_2_4_f71035d_1426101837248_396898_2228"
      property "Type", :type => "ElectionResults::ElectionType", :lower => 1, :id => "_17_0_2_4_f71035d_1426101886743_683410_2236"
    end
    klass "ElectionAdministration", :id => "_18_0_2_6340208_1441311877439_710008_4433" do
      property nil, :type => "ElectionResults::ContactInformation", :aggregation => :composite, :id => "_18_0_2_6340208_1441312459706_787590_4464"
      property "ElectionOfficialPerson", :type => "ElectionResults::Person", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1441312523523_377380_4513"
      property "Name", :type => "Gui_Builder_Profile::RichText", :id => "_18_0_2_6340208_1441312432223_272740_4455"
      property nil, :type => "ElectionResults::ReportingUnit", :is_navigable => false, :id => "_18_0_2_6340208_1441312768897_19641_4553"
    end
    klass "ElectionReport", :id => "_17_0_2_4_78e0236_1389366195564_913164_2300", :documentation => "The root class/element; attributes pertain to the status and format of the report and when generated." do
      applied_stereotype :instance_of => "XML Schema::Root"
      property nil, :type => "ElectionResults::Election", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1426102320351_976615_2363"
      property "ExternalIdentifier", :type => "ElectionResults::ExternalIdentifier", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1430412040553_669909_2247"
      property "Format", :type => "ElectionResults::ReportDetailLevel", :lower => 1, :id => "_17_0_2_4_d420315_1392318153856_710707_2448", :documentation => "Format of the results, e.g., contest summary, precinct level results, etc."
      property "GeneratedDate", :type => "ElectionResults::DateTimeWithZone", :lower => 1, :id => "_17_0_2_4_78e0236_1389733247429_431211_3338", :documentation => "Identifies the time that the election report was generated."
      property nil, :type => "ElectionResults::GpUnit", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1426788982595_725441_2719"
      property "IsTest", :type => "Primitives (Class based)::xsd::boolean", :id => "_18_0_2_6340208_1425917205849_590264_4699"
      property "Issuer", :type => "Gui_Builder_Profile::RichText", :lower => 1, :id => "_17_0_5_1_43401a7_1394578590416_259347_3759"
      property "IssuerAbbreviation", :type => "Gui_Builder_Profile::RichText", :lower => 1, :id => "_17_0_2_4_f71035d_1426542944036_608477_2211"
      property "Notes", :type => "Gui_Builder_Profile::RichText", :id => "_17_0_2_4_f71035d_1400594737789_912202_2453"
      property nil, :type => "ElectionResults::Office", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1426788177421_963220_2552"
      property nil, :type => "ElectionResults::OfficeGroup", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1433183761792_828366_2293"
      property nil, :type => "ElectionResults::Party", :aggregation => :composite, :is_ordered => true, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1426788475880_621446_2579"
      property "Person", :type => "ElectionResults::Person", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1426788901070_281905_2692"
      property "SequenceEnd", :type => "Primitives (Class based)::xsd::integer", :lower => 1, :id => "_17_0_3_43401a7_1390917636239_792774_2880", :documentation => "Indicates the upper bound of the sequence."
      property "SequenceStart", :type => "Primitives (Class based)::xsd::integer", :lower => 1, :id => "_17_0_2_4_78e0236_1389734122703_834255_3892", :documentation => "Indicates whether this file is part of a sequence of files."
      property "Status", :type => "ElectionResults::ResultsStatus", :lower => 1, :id => "_17_0_2_4_78e0236_1389734118887_523907_3888", :documentation => "Status of the election report, e.g., test mode, unofficial, etc."
      property "TestType", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1428427515312_561619_2215"
      property "VendorApplicationId", :type => "Primitives (Class based)::xsd::string", :lower => 1, :id => "_17_0_2_4_78e0236_1389733233791_999255_3335", :documentation => "An identifier of the vendor application generating the election report, e.g., X-EMS version 3.1.a."
    end
    klass "ExternalIdentifier", :id => "_17_0_2_4_f71035d_1430405712653_451634_2410" do
      property "Label", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1441215385623_864674_2521" do
        applied_stereotype :instance_of => "XML Schema::xmlAttribute"
      end
      property "OtherType", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1430405732252_109247_2429"
      property "Type", :type => "ElectionResults::IdentifierType", :lower => 1, :id => "_17_0_2_4_f71035d_1430405763078_743585_2433"
      property "Value", :type => "Primitives (Class based)::xsd::string", :lower => 1, :id => "_17_0_2_4_f71035d_1430405785820_123111_2437"
    end
    klass "GpUnit", :id => "_17_0_2_4_78e0236_1389366233346_42391_2380", :documentation => "Class/element for describing a geo-politically bounded area of geography such as a city, district, or jurisdiction, or a precinct or split-precinct, or specific vote-capture device, for the purpose of associating contest vote counts and ballot counts (and other information) with the reporting unit. Reporting units can link to each other to form a hierarchicallly-oriented model of a state's (or a county's, etc.) jurisdictions, districts, and precincts.", :is_abstract => true do
      property nil, :type => "ElectionResults::BallotStyle", :is_navigable => false, :id => "_17_0_2_4_f71035d_1436214223050_896129_2516"
      property "ComposingGpUnit", :type => "ElectionResults::GpUnit", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1442233726833_338497_2238"
      property nil, :type => "ElectionResults::Counts", :is_navigable => false, :id => "_17_0_2_4_78e0236_1389372033343_628513_2883"
      property nil, :type => "ElectionResults::ElectionReport", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426788982595_803482_2720"
      property "ExternalIdentifier", :type => "ElectionResults::ExternalIdentifier", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1430412120441_638024_2251"
      property nil, :type => "ElectionResults::GpUnit", :is_navigable => false, :id => "_17_0_2_4_f71035d_1442233726834_146073_2239"
      property "Name", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_d420315_1393450176252_314086_2868", :documentation => "Name of the reporting unit."
      property nil, :type => "ElectionResults::OtherCounts", :is_navigable => false, :id => "_18_0_2_6340208_1508176572777_556284_4626"
    end
    klass "Hours", :id => "_18_0_2_6340208_1427122205989_885563_4602" do
      property "Day", :type => "ElectionResults::DayType", :id => "_18_0_2_6340208_1427123259576_729129_4765"
      property "EndTime", :type => "ElectionResults::TimeWithZone", :lower => 1, :id => "_18_0_2_6340208_1427122318779_390600_4652"
      property "Label", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1441215533034_678840_2525" do
        applied_stereotype :instance_of => "XML Schema::xmlAttribute"
      end
      property nil, :type => "ElectionResults::Schedule", :is_navigable => false, :id => "_18_0_2_6340208_1427122219893_765770_4629"
      property "StartTime", :type => "ElectionResults::TimeWithZone", :lower => 1, :id => "_18_0_2_6340208_1427122284481_637314_4650"
    end
    klass "InternationalizedText", :id => "_17_0_2_4_f71035d_1428953680097_700602_2220" do
      property "Label", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1441215669264_408334_2533" do
        applied_stereotype :instance_of => "XML Schema::xmlAttribute"
      end
      property "Text", :type => "ElectionResults::LanguageString", :aggregation => :composite, :lower => 1, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1428953680100_198341_2225"
    end
    klass "LanguageString", :id => "_17_0_2_4_f71035d_1428953680095_709464_2219" do
      property "Content", :type => "Primitives (Class based)::xsd::string", :lower => 1, :id => "_17_0_2_4_f71035d_1428953680098_437370_2221" do
        applied_stereotype :instance_of => "XML Schema::simpleContent"
      end
      property nil, :type => "ElectionResults::InternationalizedText", :is_navigable => false, :id => "_17_0_2_4_f71035d_1428953680102_554788_2233"
      property "Language", :type => "Primitives (Class based)::xsd::language", :lower => 1, :id => "_17_0_2_4_f71035d_1428953680098_683534_2222"
    end
    klass "LatLng", :id => "_17_0_2_4_f71035d_1443104838926_393729_2222" do
      property nil, :type => "ElectionResults::ContactInformation", :is_navigable => false, :id => "_17_0_2_4_f71035d_1443105009955_327305_2262"
      property "Label", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1443104953915_703386_2253" do
        applied_stereotype :instance_of => "XML Schema::xmlAttribute"
      end
      property "Latitude", :type => "Primitives (Class based)::xsd::float", :lower => 1, :id => "_17_0_2_4_f71035d_1443104855995_933877_2241"
      property "Longitude", :type => "Primitives (Class based)::xsd::float", :lower => 1, :id => "_17_0_2_4_f71035d_1443104888887_851843_2245"
      property "Source", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1443104920563_575015_2249"
    end
    klass "Office", :id => "_17_0_5_1_43401a7_1400623830572_164081_3518" do
      property "ContactInformation", :type => "ElectionResults::ContactInformation", :aggregation => :composite, :id => "_17_0_2_4_f71035d_1429207941244_121672_2262"
      property "Contest", :type => "ElectionResults::CandidateContest", :is_navigable => false, :id => "_17_0_5_1_43401a7_1400624734486_26719_3700"
      property "Description", :type => "ElectionResults::InternationalizedText", :id => "_18_0_2_6340208_1498658815063_675104_4595"
      property nil, :type => "ElectionResults::ElectionReport", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426788177421_211291_2553"
      property "ElectoralDistrict", :type => "ElectionResults::ReportingUnit", :id => "_17_0_5_1_43401a7_1400701616170_933421_3684"
      property "ExternalIdentifier", :type => "ElectionResults::ExternalIdentifier", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1430411943269_859941_2238"
      property "FilingDeadline", :type => "Primitives (Class based)::xsd::date", :id => "_17_0_2_4_f71035d_1400610269818_58929_2680"
      property "IsPartisan", :type => "Primitives (Class based)::xsd::boolean", :id => "_17_0_2_4_f71035d_1400610117397_796726_2672"
      property "Name", :type => "ElectionResults::InternationalizedText", :lower => 1, :id => "_17_0_5_1_43401a7_1400701703746_703309_3706"
      property nil, :type => "ElectionResults::OfficeGroup", :is_navigable => false, :id => "_17_0_2_4_f71035d_1433183725966_600298_2267"
      property "OfficeHolderPerson", :type => "ElectionResults::Person", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1429177738058_248218_2294"
      property nil, :type => "ElectionResults::RetentionContest", :is_navigable => false, :id => "_18_0_2_6340208_1425646257225_938757_4581"
      property nil, :type => "ElectionResults::Term", :aggregation => :composite, :id => "_17_0_2_4_f71035d_1428489163406_781378_2243"
    end
    klass "OfficeGroup", :id => "_17_0_2_4_f71035d_1433183615993_866714_2239" do
      property nil, :type => "ElectionResults::ElectionReport", :is_navigable => false, :id => "_17_0_2_4_f71035d_1433183761792_285048_2294"
      property "Label", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1441363295584_2457_2492" do
        applied_stereotype :instance_of => "XML Schema::xmlAttribute"
      end
      property "Name", :type => "Primitives (Class based)::xsd::string", :lower => 1, :id => "_17_0_2_4_f71035d_1433183632661_693280_2258"
      property nil, :type => "ElectionResults::Office", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1433183725966_939706_2266"
      property nil, :type => "ElectionResults::OfficeGroup", :is_navigable => false, :id => "_17_0_2_4_f71035d_1433429126207_758556_2536"
      property "SubOfficeGroup", :type => "ElectionResults::OfficeGroup", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1433429126207_828493_2535"
    end
    klass "OrderedContest", :id => "_17_0_3_43401a7_1394476416139_808596_3142" do
      property nil, :type => "ElectionResults::BallotStyle", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426189065874_694421_2490"
      property "Contest", :type => "ElectionResults::Contest", :lower => 1, :id => "_17_0_3_43401a7_1394477833535_563732_3249"
      property "OrderedBallotSelection", :type => "ElectionResults::BallotSelection", :is_ordered => true, :upper => Float::INFINITY, :id => "_17_0_3_43401a7_1394477871277_951066_3270"
    end
    klass "OtherCounts", :id => "_18_0_2_6340208_1508176198256_527421_4561" do
      property nil, :type => "ElectionResults::Contest", :is_navigable => false, :id => "_17_0_2_4_78e0236_1397059165387_750452_2444"
      property "DeviceClass", :type => "ElectionResults::DeviceClass", :id => "_18_0_2_6340208_1508176283326_821025_4589"
      property nil, :type => "ElectionResults::GpUnit", :lower => 1, :id => "_18_0_2_6340208_1508176572777_602711_4625"
      property "Overvotes", :type => "Primitives (Class based)::xsd::float", :id => "_17_0_2_4_78e0236_1397155833926_751839_2443"
      property "Undervotes", :type => "Primitives (Class based)::xsd::float", :id => "_17_0_2_4_78e0236_1397155803428_746302_2439"
      property "WriteIns", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_78e0236_1397155839370_24420_2447"
    end
    klass "Party", :id => "_17_0_2_4_78e0236_1389366278128_412819_2460", :documentation => "Class/element for describing information about a political party.  Can also be a type of ballot selection for tracking a count of straight party selections." do
      property "Abbreviation", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_78e0236_1389734813018_766516_3987", :documentation => "Short name for the party, e.g., \"DEM\"."
      property nil, :type => "ElectionResults::BallotStyle", :is_navigable => false, :id => "_18_0_2_6340208_1427483833143_798813_4566"
      property nil, :type => "ElectionResults::Candidate", :is_navigable => false, :id => "_17_0_2_4_78e0236_1389366597377_496785_2699"
      property nil, :type => "ElectionResults::CandidateContest", :is_navigable => false, :id => "_17_0_2_4_78e0236_1389735000217_903057_4017"
      property nil, :type => "ElectionResults::CandidateSelection", :is_navigable => false, :id => "_17_0_2_4_d420315_1391370669921_979852_2560"
      property nil, :type => "ElectionResults::Coalition", :is_navigable => false, :id => "_18_0_2_6340208_1425647321122_252928_4745"
      property "Color", :type => "ElectionResults::HtmlColorString", :id => "_18_0_2_6340208_1425913135379_377945_4658"
      property nil, :type => "ElectionResults::ElectionReport", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426788475880_755867_2580"
      property "ExternalIdentifier", :type => "ElectionResults::ExternalIdentifier", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1430412372015_749476_2263"
      property "IsWriteIn", :type => "Primitives (Class based)::xsd::boolean", :id => "_18_0_2_6340208_1498658977530_13951_4599"
      property "LeaderPerson", :type => "ElectionResults::Person", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1506626085733_481329_4570"
      property "LogoUri", :type => "Primitives (Class based)::xsd::anyURI", :id => "_18_0_2_6340208_1425913456780_607661_4662"
      property "Name", :type => "ElectionResults::InternationalizedText", :lower => 1, :id => "_17_0_2_4_78e0236_1389710882517_230322_2174", :documentation => "Official full name of the party, e.g., \"Republican\"."
      property nil, :type => "ElectionResults::PartyRegistration", :is_navigable => false, :id => "_17_0_2_4_78e0236_1394566867126_548112_2852"
      property nil, :type => "ElectionResults::PartySelection", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426520590194_322005_2565"
      property nil, :type => "ElectionResults::Person", :is_navigable => false, :id => "_17_0_5_1_43401a7_1400673254137_213306_3703"
    end
    klass "PartyContest", :parents => ["ElectionResults::Contest"], :id => "_17_0_2_4_d420315_1393514218965_55008_3144", :documentation => "Class/element used to treat straight party selection as a type of contest.  Used so as to keep a count of straight party selections."
    klass "PartyRegistration", :id => "_17_0_2_4_78e0236_1394566839296_58362_2826" do
      property "Count", :type => "Primitives (Class based)::xsd::integer", :lower => 1, :id => "_17_0_2_4_78e0236_1394566847763_82144_2845"
      property "Party", :type => "ElectionResults::Party", :lower => 1, :id => "_17_0_2_4_78e0236_1394566867126_871059_2851"
      property nil, :type => "ElectionResults::ReportingUnit", :is_navigable => false, :id => "_17_0_2_4_f71035d_1409141976969_821908_2556"
    end
    klass "PartySelection", :parents => ["ElectionResults::BallotSelection"], :id => "_17_0_2_4_f71035d_1426519980658_594892_2511" do
      property "Party", :type => "ElectionResults::Party", :lower => 1, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1426520590194_384550_2564"
    end
    klass "Person", :id => "_17_0_5_1_43401a7_1400623980732_100904_3567" do
      property nil, :type => "ElectionResults::Candidate", :is_navigable => false, :id => "_17_0_5_1_43401a7_1400624143347_340058_3605"
      property nil, :type => "ElectionResults::ContactInformation", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1409158807179_27867_2211"
      property "DateOfBirth", :type => "Primitives (Class based)::xsd::date", :id => "_18_0_2_6340208_1425648435033_458578_4893"
      property nil, :type => "ElectionResults::ElectionAdministration", :is_navigable => false, :id => "_18_0_2_6340208_1441312523524_39228_4514"
      property nil, :type => "ElectionResults::ElectionReport", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426788901070_60377_2693"
      property "ExternalIdentifier", :type => "ElectionResults::ExternalIdentifier", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1498659439219_798023_4630"
      property "FirstName", :type => "Gui_Builder_Profile::RichText", :id => "_17_0_2_4_f71035d_1400615204010_564458_2712"
      property "FullName", :type => "ElectionResults::InternationalizedText", :id => "_17_0_2_4_f71035d_1430494058940_208317_2499"
      property "Gender", :type => "Gui_Builder_Profile::RichText", :id => "_17_0_2_4_f71035d_1434470206427_617716_2233"
      property "LastName", :type => "Gui_Builder_Profile::RichText", :id => "_17_0_2_4_78e0236_1389710872821_553533_2172", :documentation => "Candidate's last name."
      property "LeaderOfParty", :type => "ElectionResults::Party", :is_navigable => false, :id => "_18_0_2_6340208_1506626085733_742639_4571"
      property "MiddleName", :type => "Gui_Builder_Profile::RichText", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1400615257828_340032_2716"
      property "Nickname", :type => "Gui_Builder_Profile::RichText", :id => "_17_0_2_4_f71035d_1401280254171_19718_2456"
      property nil, :type => "ElectionResults::Office", :is_navigable => false, :id => "_17_0_2_4_f71035d_1429177738058_372465_2295"
      property "Party", :type => "ElectionResults::Party", :id => "_17_0_5_1_43401a7_1400673254137_48726_3702"
      property "Prefix", :type => "Gui_Builder_Profile::RichText", :id => "_17_0_5_1_43401a7_1400674672870_670385_3783"
      property "Profession", :type => "ElectionResults::InternationalizedText", :id => "_17_0_5_1_43401a7_1400673145437_284424_3693"
      property nil, :type => "ElectionResults::ReportingUnit", :is_navigable => false, :id => "_17_0_5_1_43401a7_1400624362436_508341_3663"
      property "Suffix", :type => "Gui_Builder_Profile::RichText", :id => "_17_0_2_4_f71035d_1400615284895_343066_2720"
      property "Title", :type => "ElectionResults::InternationalizedText", :id => "_17_0_5_1_43401a7_1400624817153_655858_3721"
    end
    klass "ReportingDevice", :parents => ["ElectionResults::GpUnit"], :id => "_17_0_2_4_78e0236_1389798013459_389380_4178", :documentation => "Class/element describing a specific vote-capture device." do
      property "DeviceClass", :type => "ElectionResults::DeviceClass", :id => "_17_0_2_4_f71035d_1430428476569_589857_2241"
      property "SerialNumber", :type => "Gui_Builder_Profile::RichText", :id => "_17_0_2_4_d420315_1393446014406_394266_2688", :documentation => "Device's serial number of other unique identifier."
    end
    klass "ReportingUnit", :parents => ["ElectionResults::GpUnit"], :id => "_17_0_2_4_f71035d_1400606476166_735297_2593" do
      property "Authority", :type => "ElectionResults::Person", :upper => Float::INFINITY, :id => "_17_0_5_1_43401a7_1400624362436_113642_3662"
      property "ContactInformation", :type => "ElectionResults::ContactInformation", :aggregation => :composite, :id => "_17_0_2_4_f71035d_1429207854277_960649_2231"
      property nil, :type => "ElectionResults::Contest", :is_navigable => false, :id => "_17_0_2_4_78e0236_1389366667508_288382_2752"
      property "CountStatus", :type => "ElectionResults::CountStatus", :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1430754449802_75794_2264"
      property nil, :type => "ElectionResults::Election", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426102211616_590886_2332"
      property nil, :type => "ElectionResults::ElectionAdministration", :aggregation => :composite, :id => "_18_0_2_6340208_1441312768897_951923_4552"
      property "IsDistricted", :type => "Primitives (Class based)::xsd::boolean", :id => "_17_0_2_4_f71035d_1441207733430_83517_2240"
      property "IsMailOnly", :type => "Primitives (Class based)::xsd::boolean", :id => "_17_0_2_4_f71035d_1443123936727_89395_2220"
      property "Number", :type => "Primitives (Class based)::xsd::string", :id => "_18_0_2_6340208_1497894720963_498162_4611", :documentation => "A number associated with the reporting unit (e.g. a precinct number); for compatibility with VIP."
      property nil, :type => "ElectionResults::Office", :is_navigable => false, :id => "_17_0_5_1_43401a7_1400701616170_761517_3685"
      property "OtherType", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1426007519161_685921_2510"
      property nil, :type => "ElectionResults::PartyRegistration", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_2_4_f71035d_1409141976968_835708_2555"
      property nil, :type => "ElectionResults::SpatialDimension", :aggregation => :composite, :id => "_17_0_2_4_f71035d_1426084480956_43890_2738"
      property "SubUnitsReported", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_d420315_1393507535261_72915_3031", :documentation => "For reporting unit jurisdictions, number of associated precincts that have completed reporting."
      property "TotalSubUnits", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_d420315_1393507564958_992105_3035", :documentation => "For reporting unit jurisdictions, total number of associated precincts."
      property "Type", :type => "ElectionResults::ReportingUnitType", :lower => 1, :id => "_17_0_2_4_78e0236_1389713376966_77071_2393", :documentation => "Type of reporting unit, e.g., state, jurisdiction, district, etc.\n\nThis field is a key into the NIST maintained registry of GpUnit types.\nThe key specifies the geo-political category of the locality, the type of locality, and optionally a sub-type.\n\nIf an 'Other' type or subtype is specified, then it will be defined via the OtherType value."
      property "VotersParticipated", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_f71035d_1409163555614_991016_2207"
      property "VotersRegistered", :type => "Primitives (Class based)::xsd::integer", :id => "_17_0_2_4_78e0236_1389730517829_705754_2675", :documentation => "Number of registered voters residing within the boundaries of the reporting unit."
    end
    klass "RetentionContest", :parents => ["ElectionResults::BallotMeasureContest"], :id => "_18_0_2_6340208_1425646217522_163181_4554" do
      property "Candidate", :type => "ElectionResults::Candidate", :id => "_18_0_2_6340208_1425646466278_708197_4616"
      property "Office", :type => "ElectionResults::Office", :id => "_18_0_2_6340208_1425646257224_14886_4580"
    end
    klass "Schedule", :id => "_18_0_2_6340208_1427122121448_198970_4547" do
      property nil, :type => "ElectionResults::ContactInformation", :is_navigable => false, :id => "_17_0_2_4_f71035d_1429176643252_169668_2231"
      property "EndDate", :type => "Primitives (Class based)::xsd::date", :id => "_18_0_2_6340208_1427122178673_228183_4597"
      property nil, :type => "ElectionResults::Hours", :aggregation => :composite, :upper => Float::INFINITY, :id => "_18_0_2_6340208_1427122219893_697937_4628"
      property "IsOnlyByAppointment", :type => "Primitives (Class based)::xsd::boolean", :id => "_18_0_2_6340208_1427122382178_611115_4660"
      property "IsOrByAppointment", :type => "Primitives (Class based)::xsd::boolean", :id => "_18_0_2_6340208_1427122372629_379532_4658"
      property "IsSubjectToChange", :type => "Primitives (Class based)::xsd::boolean", :id => "_18_0_2_6340208_1427122390387_8752_4662"
      property "Label", :type => "Primitives (Class based)::xsd::string", :id => "_17_0_2_4_f71035d_1441362119057_984284_2224" do
        applied_stereotype :instance_of => "XML Schema::xmlAttribute"
      end
      property "StartDate", :type => "Primitives (Class based)::xsd::date", :id => "_18_0_2_6340208_1427122157536_969193_4595"
    end
    klass "SpatialDimension", :id => "_17_0_2_4_f71035d_1407165065674_39189_2188" do
      property "MapUri", :type => "Primitives (Class based)::xsd::anyURI", :id => "_17_0_2_4_f71035d_1407172954401_413137_2508"
      property nil, :type => "ElectionResults::ReportingUnit", :is_navigable => false, :id => "_17_0_2_4_f71035d_1426084480956_314639_2739"
      property nil, :type => "ElectionResults::SpatialExtent", :aggregation => :composite, :id => "_17_0_2_4_f71035d_1409080509711_839030_2261"
    end
    klass "SpatialExtent", :id => "_17_0_2_4_f71035d_1409080246279_778720_2209" do
      property "Coordinates", :type => "Gui_Builder_Profile::RichText", :lower => 1, :id => "_17_0_2_4_f71035d_1409080350911_799955_2232"
      property "Format", :type => "ElectionResults::GeoSpatialFormat", :lower => 1, :id => "_17_0_2_4_f71035d_1409080287789_708966_2228"
      property nil, :type => "ElectionResults::SpatialDimension", :is_navigable => false, :id => "_17_0_2_4_f71035d_1409080509711_420107_2262"
    end
    klass "Term", :id => "_17_0_2_4_f71035d_1428489072598_282236_2217" do
      property "EndDate", :type => "Primitives (Class based)::xsd::date", :id => "_17_0_2_4_f71035d_1400610488881_890128_2692"
      property "Label", :type => "Primitives (Class based)::xsd::string", :id => "_18_0_2_6340208_1505852448141_524598_4584" do
        applied_stereotype :instance_of => "XML Schema::xmlAttribute"
      end
      property nil, :type => "ElectionResults::Office", :is_navigable => false, :id => "_17_0_2_4_f71035d_1428489163407_121972_2244"
      property "StartDate", :type => "Primitives (Class based)::xsd::date", :id => "_17_0_2_4_f71035d_1400610432611_11829_2688"
      property "Type", :type => "ElectionResults::OfficeTermType", :id => "_17_0_2_4_f71035d_1400610203299_137168_2676"
    end
    klass "VoteCounts", :parents => ["ElectionResults::Counts"], :id => "_17_0_2_4_78e0236_1397156604549_15838_2489" do
      property nil, :type => "ElectionResults::BallotSelection", :is_navigable => false, :id => "_17_0_2_4_78e0236_1389372026000_739694_2861"
      property "Count", :type => "Primitives (Class based)::xsd::float", :lower => 1, :id => "_17_0_2_4_78e0236_1389710697333_279003_2167", :documentation => "Count of contest votes cast; can include a factional component in special cases."
    end
    enumeration "BallotMeasureType", :id => "_17_0_2_4_f71035d_1426549604222_56408_2487" do
      property "value", :type => "UML::String"
      literal "ballot-measure", :id => "_17_0_2_4_f71035d_1426549624051_325504_2505"
      literal "initiative", :id => "_17_0_2_4_f71035d_1426549642875_810873_2507"
      literal "other", :id => "_17_0_2_4_f71035d_1426549671283_243982_2511"
      literal "recall", :id => "_18_0_5_43401a7_1508329822727_578792_4301"
      literal "referendum", :id => "_17_0_2_4_f71035d_1426549650643_862674_2509"
    end
    enumeration "CandidatePostElectionStatus", :id => "_17_0_2_4_78e0236_1389797791548_146399_4136", :documentation => "Enumerations for various status applicable to a candidate in Candidate, e.g., filed, winner, etc." do
      property "value", :type => "UML::String"
      literal "advanced-to-runoff", :id => "_17_0_2_4_78e0236_1390498103083_612885_2204", :documentation => "For ranked order voting."
      literal "defeated", :id => "_18_0_2_6340208_1522862436551_713511_4555"
      literal "projected-winner", :id => "_17_0_2_4_f71035d_1426779343856_684313_2527"
      literal "winner", :id => "_17_0_2_4_78e0236_1390498122756_845764_2207"
      literal "withdrawn", :id => "_17_0_2_4_78e0236_1389797808039_882240_4158"
    end
    enumeration "CandidatePreElectionStatus", :id => "_17_0_2_4_f71035d_1427223542780_950918_2213" do
      property "value", :type => "UML::String"
      literal "filed", :id => "_17_0_2_4_f71035d_1427223640266_788861_2231"
      literal "qualified", :id => "_17_0_2_4_f71035d_1427223649963_617948_2233"
      literal "withdrawn", :id => "_17_0_2_4_f71035d_1427223661291_298091_2235"
    end
    enumeration "CountItemStatus", :id => "_17_0_2_4_78e0236_1389797161173_369293_4078" do
      property "value", :type => "UML::String"
      literal "completed", :id => "_17_0_2_4_78e0236_1389797202891_585067_4096"
      literal "in-process", :id => "_17_0_2_4_78e0236_1389797215251_433372_4098"
      literal "not-processed", :id => "_17_0_2_4_78e0236_1389797220705_357480_4100"
      literal "unknown", :id => "_17_0_2_4_d420315_1393511725903_432125_3094"
    end
    enumeration "CountItemType", :id => "_17_0_2_4_78e0236_1389798097477_664878_4228", :documentation => "Enumerations for classes of voting associated with votes in Votes, e.g., absentee, election day, early, etc." do
      property "value", :type => "UML::String"
      literal "absentee", :id => "_17_0_2_4_78e0236_1389798158958_823976_4262", :documentation => "For any/all classes of absentee, generally when absentee is not broken out into specific classes."
      literal "absentee-fwab", :id => "_17_0_2_4_f71035d_1403260880887_328465_2190"
      literal "absentee-in-person", :id => "_17_0_2_4_d420315_1393444752746_777145_2663", :documentation => "A class of absentee; for absentee ballots cast in-person, e.g., at a county office."
      literal "absentee-mail", :id => "_17_0_2_4_d420315_1393444686544_829364_2661", :documentation => "A class of absentee; for postal mail absentee ballots separately."
      literal "early", :id => "_17_0_2_4_78e0236_1389798152548_861275_4260"
      literal "election-day", :id => "_17_0_2_4_78e0236_1389798146104_554367_4258"
      literal "other", :id => "_17_0_2_4_78e0236_1389798167680_247144_4266"
      literal "provisional", :id => "_17_0_2_4_78e0236_1389798164517_576839_4264"
      literal "seats", :id => "_18_0_2_6340208_1518035681133_157938_4554", :documentation => "for legislative balance-of-power results information"
      literal "total", :id => "_17_0_2_4_d420315_1393443626907_186632_2636", :documentation => "Total of all ballots cast regardless of voting class."
      literal "uocava", :id => "_17_0_2_4_d420315_1393442606355_440980_2634", :documentation => "A class of absentee; for absentee ballots from UOCAVA voters."
      literal "write-in", :id => "_17_0_2_4_f71035d_1426542406558_659527_2207"
    end
    enumeration "DayType", :id => "_18_0_2_6340208_1425647845906_917814_4818" do
      property "value", :type => "UML::String"
      literal "all", :id => "_18_0_2_6340208_1425648118607_235416_4887"
      literal "friday", :id => "_18_0_2_6340208_1425647878776_463447_4845"
      literal "monday", :id => "_18_0_2_6340208_1425647855134_998400_4837"
      literal "saturday", :id => "_18_0_2_6340208_1425647881824_34266_4847"
      literal "sunday", :id => "_18_0_2_6340208_1425647888198_705727_4849"
      literal "thursday", :id => "_18_0_2_6340208_1425647870130_428637_4843"
      literal "tuesday", :id => "_18_0_2_6340208_1425647860730_71688_4839"
      literal "wednesday", :id => "_18_0_2_6340208_1425647866276_681465_4841"
      literal "weekday", :id => "_18_0_2_6340208_1425647889758_954217_4851"
      literal "weekend", :id => "_18_0_2_6340208_1425647894225_692230_4853"
    end
    enumeration "DeviceType", :id => "_17_0_2_4_78e0236_1389798087342_91702_4210", :documentation => "Enumerations for type of device in Device and for type of device associated with vote counts in Votes." do
      property "value", :type => "UML::String"
      literal "electronic", :id => "_17_0_2_4_78e0236_1389798119806_329053_4250"
      literal "lever", :id => "_17_0_2_4_78e0236_1389798130868_631503_4254"
      literal "manual-count", :id => "_17_0_2_4_78e0236_1389798122729_51972_4252", :documentation => "For hand-counted paper ballots or other ballots manually counted."
      literal "mixed-systems", :id => "_17_0_3_43401a7_1395676480784_72892_3144"
      literal "opscan-central", :id => "_17_0_2_4_78e0236_1389798116792_251263_4248"
      literal "opscan-precinct", :id => "_17_0_2_4_78e0236_1389798109626_261027_4246"
      literal "other", :id => "_17_0_2_4_f71035d_1426078465747_611474_2669"
      literal "punch-card", :id => "_17_0_2_4_78e0236_1389798140096_817289_4256"
      literal "unknown", :id => "_17_0_2_4_d420315_1393443777699_888607_2638"
    end
    enumeration "ElectionType", :id => "_17_0_2_4_78e0236_1389734457182_720347_3938" do
      property "value", :type => "UML::String"
      literal "general", :id => "_17_0_2_4_78e0236_1389734485163_78530_3962"
      literal "other", :id => "_17_0_5_1_43401a7_1395052937250_824092_3516"
      literal "partisan-primary-closed", :id => "_17_0_2_4_78e0236_1389734467766_324825_3956"
      literal "partisan-primary-open", :id => "_17_0_2_4_78e0236_1389734474376_633780_3958"
      literal "primary", :id => "_17_0_2_4_f71035d_1400604260784_726426_2505"
      literal "runoff", :id => "_17_0_2_4_78e0236_1389734488059_492169_3964"
      literal "special", :id => "_17_0_2_4_78e0236_1389734479743_454576_3960"
    end
    enumeration "GeoSpatialFormat", :id => "_17_0_2_4_f71035d_1425325534467_889921_2544" do
      property "value", :type => "UML::String"
      literal "geo-json", :id => "_17_0_2_4_f71035d_1425325554296_227692_2562"
      literal "gml", :id => "_17_0_2_4_f71035d_1425325658423_23072_2564"
      literal "kml", :id => "_17_0_2_4_f71035d_1425325669323_898515_2566"
      literal "shp", :id => "_17_0_2_4_f71035d_1425325695625_410292_2570"
      literal "wkt", :id => "_17_0_2_4_f71035d_1425325676288_248577_2568"
    end
    enumeration "IdentifierType", :id => "_17_0_2_4_f71035d_1425061188508_163854_2613" do
      property "value", :type => "UML::String"
      literal "fips", :id => "_17_0_2_4_f71035d_1425061241086_739613_2631"
      literal "local-level", :id => "_17_0_2_4_f71035d_1425061272416_498928_2635"
      literal "national-level", :id => "_17_0_2_4_f71035d_1425061308144_642254_2639"
      literal "ocd-id", :id => "_17_0_2_4_f71035d_1425061253888_677563_2633"
      literal "other", :id => "_17_0_2_4_f71035d_1425061332140_426311_2641"
      literal "state-level", :id => "_17_0_2_4_f71035d_1425061292620_153404_2637"
    end
    enumeration "OfficeTermType", :id => "_17_0_2_4_f71035d_1425314816880_411605_2504" do
      property "value", :type => "UML::String"
      literal "full-term", :id => "_17_0_2_4_f71035d_1425314848544_236391_2522"
      literal "unexpired-term", :id => "_17_0_2_4_f71035d_1425314870188_959725_2524"
    end
    enumeration "ReportDetailLevel", :id => "_17_0_2_4_d420315_1392318380928_311473_2471" do
      property "value", :type => "UML::String"
      literal "precinct-level", :id => "_17_0_2_4_d420315_1392318883141_144733_2499"
      literal "summary-contest", :id => "_17_0_2_4_d420315_1392318478070_728065_2491"
    end
    enumeration "ReportingUnitType", :id => "_17_0_2_4_f71035d_1431607637366_785815_2242" do
      property "value", :type => "UML::String"
      literal "ballot-batch", :id => "_17_0_2_4_f71035d_1441114658492_609849_2529"
      literal "ballot-style-area", :id => "_17_0_2_4_f71035d_1443124003815_739653_2224"
      literal "borough", :id => "_17_0_2_4_f71035d_1446109817045_28569_2509"
      literal "city", :id => "_17_0_2_4_f71035d_1431607678535_652600_2264"
      literal "city-council", :id => "_17_0_2_4_f71035d_1441114667618_868300_2531"
      literal "combined-precinct", :id => "_17_0_2_4_f71035d_1441114700100_811167_2533"
      literal "congressional", :id => "_17_0_2_4_f71035d_1441114755212_383591_2535"
      literal "country", :id => "_18_0_2_6340208_1498659495271_27294_4634"
      literal "county", :id => "_17_0_2_4_f71035d_1441114781732_167572_2537"
      literal "county-council", :id => "_17_0_2_4_f71035d_1441114788779_738591_2539"
      literal "drop-box", :id => "_17_0_2_4_f71035d_1441114797634_816991_2541"
      literal "judicial", :id => "_17_0_2_4_f71035d_1431607656912_452127_2260"
      literal "municipality", :id => "_17_0_2_4_f71035d_1441114803817_62853_2543"
      literal "other", :id => "_17_0_2_4_f71035d_1441115630759_996037_2576"
      literal "polling-place", :id => "_17_0_2_4_f71035d_1441114882623_309688_2545"
      literal "precinct", :id => "_17_0_2_4_f71035d_1441115364942_567288_2548"
      literal "school", :id => "_17_0_2_4_f71035d_1441115381430_470118_2550"
      literal "special", :id => "_17_0_2_4_f71035d_1441115393894_919432_2552"
      literal "split-precinct", :id => "_17_0_2_4_f71035d_1441115433028_554839_2554"
      literal "state", :id => "_17_0_2_4_f71035d_1441115443635_44997_2556"
      literal "state-house", :id => "_17_0_2_4_f71035d_1441115460812_273865_2560"
      literal "state-senate", :id => "_17_0_2_4_f71035d_1441115450698_21141_2558"
      literal "town", :id => "_17_0_2_4_f71035d_1441115468017_414462_2562"
      literal "township", :id => "_17_0_2_4_f71035d_1441115478563_205395_2564"
      literal "utility", :id => "_17_0_2_4_f71035d_1441115489758_945904_2566"
      literal "village", :id => "_17_0_2_4_f71035d_1441115498698_592530_2568"
      literal "vote-center", :id => "_17_0_2_4_f71035d_1441115505571_11321_2570"
      literal "ward", :id => "_17_0_2_4_f71035d_1441115513438_413272_2572"
      literal "water", :id => "_17_0_2_4_f71035d_1441115520086_128633_2574"
    end
    enumeration "ResultsStatus", :id => "_17_0_2_4_78e0236_1389734128637_37089_3895" do
      property "value", :type => "UML::String"
      literal "certified", :id => "_17_0_2_4_d420315_1393512696499_784557_3103"
      literal "correction", :id => "_17_0_2_4_78e0236_1389734159285_636213_3921"
      literal "pre-election", :id => "_17_0_2_4_78e0236_1389734135976_457148_3913"
      literal "recount", :id => "_17_0_2_4_d420315_1393512693149_270724_3101"
      literal "unofficial-complete", :id => "_17_0_2_4_78e0236_1389734149424_623797_3917"
      literal "unofficial-partial", :id => "_17_0_2_4_78e0236_1389734142410_446530_3915"
    end
    enumeration "VoteVariation", :id => "_17_0_2_4_78e0236_1389798224990_11192_4272", :documentation => "Enumerations for vote variations applicable to a contest, e.g., n-of-m, approval, etc." do
      property "value", :type => "UML::String"
      literal "1-of-m", :id => "_17_0_2_4_f71035d_1426674724266_523557_2207"
      literal "approval", :id => "_17_0_2_4_d420315_1393442101326_72012_2610", :documentation => "When voter can select as many candidates as desired in a contest up to a max number."
      literal "borda", :id => "_17_0_2_4_f71035d_1426690480498_149754_2207"
      literal "cumulative", :id => "_17_0_2_4_d420315_1393442106110_778526_2612", :documentation => "When voter can allocate more than one vote to a given candidate."
      literal "majority", :id => "_17_0_2_4_f71035d_1426705831174_143716_2207"
      literal "n-of-m", :id => "_17_0_2_4_78e0236_1389798239748_249291_4292", :documentation => "Includes vote for 1, i.e., 1-of-m."
      literal "other", :id => "_17_0_2_4_f71035d_1426537393798_302890_2801"
      literal "plurality", :id => "_17_0_2_4_f71035d_1426674800911_595890_2211"
      literal "proportional", :id => "_17_0_2_4_f71035d_1426674783966_416832_2209"
      literal "range", :id => "_17_0_2_4_f71035d_1426674991054_592562_2213"
      literal "rcv", :id => "_17_0_2_4_f71035d_1426674997905_245495_2215"
      literal "super-majority", :id => "_17_0_2_4_f71035d_1426705841212_816376_2209"
    end
    primitive "DateTimeWithZone", :parents => ["Primitives (Class based)::xsd::dateTime"], :id => "_18_0_2_6340208_1519999692422_172889_4576", :documentation => "Restricts dateTime to require inclusion of timezone information and excludes fractional seconds" do
      applied_stereotype :instance_of => "Primitives (Class based)::prim"
      property "pattern", :type => "Primitives (Class based)::xsd::string", :default_value => "[0-9]{4}-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]|(24:00:00))(Z|[+-]((0[0-9]|1[0-3]):[0-5][0-9]|14:00))", :id => "_18_0_2_6340208_1519999795210_981371_4601"
    end
    primitive "HtmlColorString", :parents => ["Primitives (Class based)::xsd::string"], :id => "_17_0_2_4_f71035d_1428586849773_722256_2252" do
      applied_stereotype :instance_of => "Primitives (Class based)::prim"
      property "pattern", :type => "UML::String", :default_value => "[0-9a-f]{6}", :id => "_17_0_2_4_f71035d_1428586849775_553802_2253"
    end
    primitive "ShortString", :parents => ["Primitives (Class based)::xsd::string"], :id => "_18_0_2_6340208_1499878618645_537953_4560" do
      applied_stereotype :instance_of => "Primitives (Class based)::prim"
      property "maxLength", :type => "UML::Integer", :default_value => 32, :id => "_18_0_2_6340208_1499878669447_998177_4582"
    end
    primitive "TimeWithZone", :parents => ["Primitives (Class based)::xsd::time"], :id => "_18_0_2_6340208_1427385616970_86952_4407", :documentation => "Restricts time to require inclusion of timezone information and excludes fractional seconds" do
      applied_stereotype :instance_of => "Primitives (Class based)::prim"
      property "pattern", :type => "Primitives (Class based)::xsd::string", :default_value => "(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]|(24:00:00))(Z|[+-]((0[0-9]|1[0-3]):[0-5][0-9]|14:00))", :id => "_18_0_2_6340208_1427385616971_659746_4409"
    end
  end
  package "Primitives (Class based)", :id => "_17_0_2_4_78e0236_1389733484658_332535_3377" do
    package "EXPRESS_Primitives", :id => "_17_0_2_4_78e0236_1389733484679_191489_3386" do
      primitive "AGGREGATE", :parents => ["Primitives (Class based)::Array"], :id => "_17_0_2_4_78e0236_1389733486714_480102_3465" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "ARRAY", :parents => ["Primitives (Class based)::Array"], :id => "_17_0_2_4_78e0236_1389733486717_126126_3471" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "BAG", :parents => ["Primitives (Class based)::Array"], :id => "_17_0_2_4_78e0236_1389733486717_494732_3472" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "BINARY", :id => "_17_0_2_4_78e0236_1389733486715_484583_3466" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "BOOLEAN", :id => "_17_0_2_4_78e0236_1389733486715_479247_3467" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "GENERIC_ENTITY", :id => "_17_0_2_4_78e0236_1389733486718_280819_3473" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "INTEGER", :id => "_17_0_2_4_78e0236_1389733486716_673592_3469" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "LIST", :parents => ["Primitives (Class based)::Array"], :id => "_17_0_2_4_78e0236_1389733486715_549906_3468" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "LOGICAL", :id => "_17_0_2_4_78e0236_1389733486714_484973_3463" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "NUMBER", :id => "_17_0_2_4_78e0236_1389733486716_206327_3470" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "REAL", :id => "_17_0_2_4_78e0236_1389733486718_571809_3474" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "SET", :parents => ["Primitives (Class based)::Array"], :id => "_17_0_2_4_78e0236_1389733486714_730777_3464" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "STRING", :id => "_17_0_2_4_78e0236_1389733486713_729576_3462" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
    end
    package "xsd", :id => "_17_0_2_4_78e0236_1389733484660_420867_3380" do
      primitive "ENTITIES", :parents => ["Primitives (Class based)::xsd::anySimpleType"], :id => "_17_0_2_4_78e0236_1389733484692_667105_3408" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "ENTITY", :parents => ["Primitives (Class based)::xsd::NCName"], :id => "_17_0_2_4_78e0236_1389733484701_545196_3425" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "ID", :parents => ["Primitives (Class based)::xsd::NCName"], :id => "_17_0_2_4_78e0236_1389733484682_115730_3393" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "IDREF", :parents => ["Primitives (Class based)::xsd::NCName"], :id => "_17_0_2_4_78e0236_1389733484698_63794_3420" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "IDREFS", :id => "_17_0_2_4_78e0236_1389733484700_317660_3422" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "NCName", :parents => ["Primitives (Class based)::xsd::Name"], :id => "_17_0_2_4_78e0236_1389733484687_643733_3400" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "NMTOKEN", :parents => ["Primitives (Class based)::xsd::token"], :id => "_17_0_2_4_78e0236_1389733484689_75975_3404" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "Name", :parents => ["Primitives (Class based)::xsd::token"], :id => "_17_0_2_4_78e0236_1389733484684_671168_3395" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "Notation", :id => "_17_0_2_4_78e0236_1389733484705_212065_3431" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "QName", :id => "_17_0_2_4_78e0236_1389733484693_457603_3410" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "anySimpleType", :parents => ["Primitives (Class based)::xsd::anyType"], :id => "_17_0_2_4_78e0236_1389733484704_780190_3430" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "anyType", :id => "_17_0_2_4_78e0236_1389733484698_421758_3418" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "anyURI", :id => "_17_0_2_4_78e0236_1389733484699_771735_3421", :documentation => "<span style=\"font-weight:normal;font-style:normal;font-variant:normal;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">A \n    W3C standard data type for hyperlinks. The W3C definition of xsd:anyURI is \n    in </span><span style=\"font-variant:normal;font-weight:normal;font-style:normal;color:#000BFF;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">http://www.w3.org/TR/xmlschema-2/</span><span style=\"font-variant:normal;font-weight:normal;font-style:normal;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">.</span>" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "base64Binary", :parents => ["Primitives (Class based)::xsd::anySimpleType"], :id => "_17_0_2_4_78e0236_1389733484694_526234_3412", :documentation => "<span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">The \n    data is encoded using base64. (see IETF </span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\"><em>RFC \n    1421 </em></span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">for \n    the base64 algorithm and </span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;color:#000BFF;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">http://www.w3.org/TR/xmlschema-2/#base64Binary</span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">)</span>" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "boolean", :id => "_17_0_2_4_78e0236_1389733484709_72835_3437" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "byte", :parents => ["Primitives (Class based)::xsd::short"], :id => "_17_0_2_4_78e0236_1389733484696_569147_3415" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "date", :id => "_17_0_2_4_78e0236_1389733484695_950073_3414" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "dateTime", :parents => ["Primitives (Class based)::xsd::anySimpleType"], :id => "_17_0_2_4_78e0236_1389733484691_275387_3406", :documentation => "<span style=\"font-weight:normal;font-style:normal;font-variant:normal;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">The \n    W3C standard data type for the current date and time is xsd:dateTime. (See </span><span style=\"font-variant:normal;font-weight:normal;font-style:normal;color:#000BFF;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">http://www.w3.org/TR/NOTE-datetime-970915.html</span><span style=\"font-variant:normal;font-weight:normal;font-style:normal;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">.) \n    The following formats from the W3C specification are recommended for 258X \n    files:    \n<br>Complete date plus hours, minutes and seconds:    \n<br>YYYY-MM-DDThh:mm:ssTZD (e.g. 1997-07-16T19:20:30.4536+01:00)    \n<br>Complete date plus hours, minutes, seconds and a decimal fraction of a \n    Second:    \n<br>YYYY-MM-DDThh:mm:ss.sTZD (e.g. 1997-07-16T19:20:30.45+01:00)    \n<br>where:    \n<br>YYYY = four-digit year    \n<br>MM = two-digit month (01=January, etc.)    \n<br>DD = two-digit day of month (01 through 31)    \n<br>Hh = two digits of hour (00 through 23) (am/pm NOT allowed)    \n<br>Mm = two digits of minute (00 through 59)    \n<br>Ss = two digits of second (00 through 59)    \n<br>S = one or more digits representing a decimal fraction of a second    \n<br>TZD = time zone designator (Z or +hh:mm or –hh:mm)</span>" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "dayTimeDuration", :parents => ["Primitives (Class based)::xsd::duration"], :id => "_17_0_2_4_78e0236_1389733484692_259230_3409" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "decimal", :parents => ["Primitives (Class based)::xsd::anySimpleType"], :id => "_17_0_2_4_78e0236_1389733484707_765513_3434" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "double", :id => "_17_0_2_4_78e0236_1389733484681_69371_3391", :documentation => "<span style=\"font-weight:normal;font-style:normal;font-variant:normal;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">A \n    W3C standard data type for a binary floating-point number. The W3C \n    definition of xsd:double is in </span><span style=\"font-variant:normal;font-style:normal;font-weight:normal;color:#000BFF;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">http://www.w3.org/TR/xmlschema-2/</span><span style=\"font-variant:normal;font-style:normal;font-weight:normal;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">.    \n<br>The xsd:double is a number where the value can be positive, negative, \n    integer or floating point, with at least 7 digits of precision. Numbers \n    are assumed to be positive but can be explicitly designated as positive by \n    preceding the number with a ‘+’ (ASCII decimal 43) character. Negative \n    numbers must be explicitly designated as negative by a preceding ‘–‘ \n    (ASCII decimal 45) character. An internal representation of an IEEE double \n    precision floating-point number is assumed. This range of values for IEEE \n    doubles is defined as 3.4x10-38 </span><span style=\"font-size:7px;\">≤ </span><span style=\"font-variant:normal;font-style:normal;font-weight:normal;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">value \n    </span><span style=\"font-size:7px;\">≤ </span><span style=\"font-variant:normal;font-style:normal;font-weight:normal;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">3.4x10+38. \n    The format for representing a double is the same as the format used in the \n    computer languages C, Perl, Python, or TCL. For example, all the following \n    are legal numbers:    \n<br>1.005 ; 0.01; .01; -2.334e-33; .224e-2</span>" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "duration", :parents => ["Primitives (Class based)::xsd::anySimpleType"], :id => "_17_0_2_4_78e0236_1389733484682_628991_3392" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "float", :id => "_17_0_2_4_78e0236_1389733484690_726252_3405" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "gDay", :id => "_17_0_2_4_78e0236_1389733484702_618400_3426" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "gMonth", :id => "_17_0_2_4_78e0236_1389733484686_309394_3399" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "gMonthDay", :parents => ["Primitives (Class based)::xsd::anySimpleType"], :id => "_17_0_2_4_78e0236_1389733484689_406587_3403" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "gYear", :id => "_17_0_2_4_78e0236_1389733484686_973909_3398" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "gYearMonth", :parents => ["Primitives (Class based)::xsd::anySimpleType"], :id => "_17_0_2_4_78e0236_1389733484701_726131_3424" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "hexBinary", :parents => ["Primitives (Class based)::xsd::anySimpleType"], :id => "_17_0_2_4_78e0236_1389733484688_526156_3401" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "int", :parents => ["Primitives (Class based)::xsd::long"], :id => "_17_0_2_4_78e0236_1389733484703_236474_3428" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "integer", :parents => ["Primitives (Class based)::xsd::decimal"], :id => "_17_0_2_4_78e0236_1389733484684_962828_3396" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "language", :parents => ["Primitives (Class based)::xsd::token"], :id => "_17_0_2_4_78e0236_1389733484688_705605_3402" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "long", :parents => ["Primitives (Class based)::xsd::integer"], :id => "_17_0_2_4_78e0236_1389733484696_920665_3416" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "negativeInteger", :parents => ["Primitives (Class based)::xsd::nonPositiveInteger"], :id => "_17_0_2_4_78e0236_1389733484700_370943_3423" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "nonNegativeInteger", :parents => ["Primitives (Class based)::xsd::integer"], :id => "_17_0_2_4_78e0236_1389733484685_653521_3397", :documentation => "<span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">A \n    W3C standard data type for non-negative integer numbers. The W3C \n    definition of xsd:nonNegativeInteger is in </span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;color:#000BFF;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">http://www.w3.org/TR/xmlschema-2/</span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">.    \n<br>The range of values allowed are 0 </span><span style=\"font-size:7px;\">≤ </span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">value \n    </span><span style=\"font-size:7px;\">≤ </span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">2147483647 \n    (the non-negative values that fit in a 32 bit signed integer).</span>" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "nonPositiveInteger", :parents => ["Primitives (Class based)::xsd::integer"], :id => "_17_0_2_4_78e0236_1389733484705_671757_3432" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "normalizedString", :parents => ["Primitives (Class based)::xsd::string"], :id => "_17_0_2_4_78e0236_1389733484695_985594_3413" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "positiveInteger", :parents => ["Primitives (Class based)::xsd::nonNegativeInteger"], :id => "_17_0_2_4_78e0236_1389733484706_284527_3433", :documentation => "<span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">A \n    W3C standard data type for positive integer numbers. The W3C definition of \n    xsd:positiveInteger is in </span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;color:#000BFF;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">http://www.w3.org/TR/xmlschema-2/</span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">.    \n<br>The range of values allowed are 1 </span><span style=\"font-size:7px;\">≤ </span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">value \n    </span><span style=\"font-size:7px;\">≤ </span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">2147483647 \n    (the positive values that fit in a 32 bit signed integer).</span>" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "precisionDecimal", :parents => ["Primitives (Class based)::xsd::anySimpleType"], :id => "_17_0_2_4_78e0236_1389733484708_421725_3436" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "short", :parents => ["Primitives (Class based)::xsd::int"], :id => "_17_0_2_4_78e0236_1389733484702_668356_3427" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "string", :parents => ["Primitives (Class based)::xsd::anySimpleType"], :id => "_17_0_2_4_78e0236_1389733484691_67945_3407", :documentation => "<span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">A \n    W3C standard data type for a Unicode character string. The characters are \n    from the UTF-8 character set as defined in </span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;color:#000BFF;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">http://www.ietf.org/rfc/rfc2279.txt</span><span style=\"font-style:normal;font-variant:normal;line-height:normal;font-weight:normal;font-size:7px;font-family:Arial, Verdana, Helvetica, sans-serif;\">.</span>" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "time", :id => "_17_0_2_4_78e0236_1389733484698_553774_3419" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "token", :parents => ["Primitives (Class based)::xsd::normalizedString"], :id => "_17_0_2_4_78e0236_1389733484681_672518_3390" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "unsignedByte", :parents => ["Primitives (Class based)::xsd::unsignedShort"], :id => "_17_0_2_4_78e0236_1389733484707_597220_3435", :documentation => "<span style=\"font-weight:normal;font-style:normal;font-variant:normal;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">The \n    W3C standard for an unsigned byte (an unsigned 8 bit integer with a value \n    between 0- 255.) The W3C definition of xsd:unsignedByte is in </span><span style=\"font-variant:normal;font-weight:normal;font-style:normal;color:#000BFF;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">http://www.w3.org/TR/xmlschema-2/</span><span style=\"font-variant:normal;font-weight:normal;font-style:normal;line-height:normal;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:7px;\">.</span>" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "unsignedInt", :parents => ["Primitives (Class based)::xsd::unsignedLong"], :id => "_17_0_2_4_78e0236_1389733484704_325090_3429" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "unsignedLong", :parents => ["Primitives (Class based)::xsd::nonNegativeInteger"], :id => "_17_0_2_4_78e0236_1389733484697_489833_3417" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "unsignedShort", :parents => ["Primitives (Class based)::xsd::unsignedInt"], :id => "_17_0_2_4_78e0236_1389733484683_113657_3394" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
      primitive "yearMonthDuration", :parents => ["Primitives (Class based)::xsd::duration"], :id => "_17_0_2_4_78e0236_1389733484693_920822_3411" do
        applied_stereotype :instance_of => "Primitives (Class based)::prim"
      end
    end
    primitive "Array", :id => "_17_0_2_4_78e0236_1389733484661_358463_3381" do
      applied_stereotype :instance_of => "Primitives (Class based)::prim"
    end
    stereotype "enum", :metaclasses => ["Class"], :id => "_17_0_2_4_78e0236_1389733484678_645478_3384"
    stereotype "prim", :metaclasses => ["Class"], :id => "_17_0_2_4_78e0236_1389733484659_95330_3378"
    stereotype "union", :metaclasses => ["Interface"], :id => "_17_0_2_4_78e0236_1389733484679_745223_3385"
  end
  profile "ArtifactGeneration", :id => "_18_0_2_6340208_1498052354318_736115_4565" do
    stereotype "generation_options", :metaclasses => ["Package"], :id => "_18_0_2_6340208_1498052707631_597828_4567" do
      tag "pluralize_role_names", :id => "_18_0_2_6340208_1498052801329_853769_4578"
      tag "snakecase_role_names", :id => "_18_0_2_6340208_1501012980570_596824_4558"
    end
  end
  profile "Gui_Builder_Profile", :id => "_9_5_1_f2e0365_1132085180552_532309_18941" do
    applied_stereotype :instance_of => "Gui_Builder_Profile::options"
    association :properties => ["_17_0_4_2_78e0236_1386346399057_474802_3400", "Gui_Builder_Profile::File::binary_data"], :id => "_17_0_4_2_78e0236_1386346399057_554755_3399"
    association :properties => ["Gui_Builder_Profile::InvitationCode::additional_email_requirements", "Gui_Builder_Profile::StringCondition::additional_email_requirement_for"], :id => "_16_9_78e0236_1362444527650_795906_1494"
    association :properties => ["Gui_Builder_Profile::InvitationCode::roles_granted", "Gui_Builder_Profile::UserRole::invitations"], :association_class => "Gui_Builder_Profile::GrantedPermissions", :id => "ac__16_9_78e0236_1361655331501_974661_1486"
    association :properties => ["_16_9_78e0236_1362000053868_346349_1709", "_16_9_78e0236_1362000053869_30585_1710"], :id => "_16_9_78e0236_1362000053868_440794_1708"
    association :properties => ["Gui_Builder_Profile::Perspective::substitutions", "_16_9_78e0236_1361999994974_761045_1678"], :id => "_16_9_78e0236_1361999994973_620448_1676"
    association :properties => ["Gui_Builder_Profile::ProjectOptions::email_requirements", "Gui_Builder_Profile::StringCondition::email_requirement_for"], :id => "_16_9_78e0236_1362435833928_585337_1539"
    association :properties => ["Gui_Builder_Profile::ProjectOptions::password_requirements", "Gui_Builder_Profile::StringCondition::password_requirement_for"], :id => "_16_9_78e0236_1362435861830_956872_1548"
    association :properties => ["Gui_Builder_Profile::RichText::images", "_17_0_4_2_78e0236_1386346294429_948280_3299"], :id => "_17_0_4_2_78e0236_1386346294427_421114_3296"
    association :properties => ["_16_9_78e0236_1362000016908_686909_1688", "_16_9_78e0236_1362000016909_933287_1689"], :id => "_16_9_78e0236_1362000016908_599111_1687"
    association :properties => ["Gui_Builder_Profile::User::roles", "Gui_Builder_Profile::UserRole::users"], :association_class => "Gui_Builder_Profile::RolePermissions", :id => "ac__16_9_78e0236_1361377547806_696994_1711"
    klass "AdminRole", :parents => ["Gui_Builder_Profile::UserRole"], :id => "_16_9_78e0236_1361377651377_358378_1748"
    klass "BinaryData", :id => "_16_9_28b0142_1350320910064_324080_1403" do
      property nil, :type => "Gui_Builder_Profile::File", :id => "_17_0_4_2_78e0236_1386346399057_474802_3400"
      property "data", :type => "UML::ByteString", :id => "_16_9_28b0142_1350322818987_206540_1405"
    end
    klass "Code", :id => "_16_9_28b0142_1305737761267_4412_1473" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "content", :type => "UML::String", :id => "_16_9_28b0142_1345232682459_819388_1563" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "language", :type => "Gui_Builder_Profile::LanguageType", :id => "_16_9_28b0142_1305737768320_72319_1474"
    end
    klass "File", :id => "_16_9_6620216_1310677254545_658641_1463" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "binary_data", :type => "Gui_Builder_Profile::BinaryData", :aggregation => :composite, :id => "_16_9_6620216_1310677254546_719122_1464"
      property "filename", :type => "UML::String", :id => "_16_9_6620216_1310677254546_312818_1465" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "mime_type", :type => "UML::String", :id => "_16_9_6620216_1310677254546_544129_1466"
    end
    klass "GrantedPermissions", :id => "_16_9_78e0236_1361655331501_974661_1486" do
      property "role_manager", :type => "UML::Boolean", :id => "_16_9_78e0236_1361655366955_945029_1526"
    end
    klass "InvitationCode", :id => "_16_9_78e0236_1361654692810_715969_1432" do
      property nil, :type => "Gui_Builder_Profile::Perspective", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000053869_30585_1710"
      property "additional_email_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1362444527651_178908_1495"
      property "code", :type => "UML::String", :id => "_16_9_78e0236_1361654722176_380927_1450" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "expires", :type => "Gui_Builder_Profile::Timestamp", :id => "_16_9_78e0236_1361654732105_93745_1452"
      property "roles_granted", :type => "Gui_Builder_Profile::UserRole", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361654888054_544647_1461"
      property "uses_remaining", :type => "UML::Integer", :id => "_16_9_78e0236_1361654751742_918976_1454"
    end
    klass "MultivocabularySubstitution", :id => "_16_9_6620216_1311349467640_10706_1828" do
      property nil, :type => "Gui_Builder_Profile::Perspective", :lower => 1, :id => "_16_9_78e0236_1361999994974_761045_1678"
      property "master_word", :type => "UML::String", :id => "_16_9_6620216_1311349486021_149646_1829"
      property "replacement_word", :type => "UML::String", :id => "_16_9_6620216_1311349518319_893941_1830"
    end
    klass "Person", :id => "_17_0_2_4_b9202e7_1402410462540_930340_2171" do
      property "email", :type => "UML::String", :id => "_16_9_78e0236_1361378907971_285264_1915" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "email_verified", :type => "UML::Boolean", :id => "_16_9_78e0236_1361378935636_408890_1917"
      property "first_name", :type => "UML::String", :id => "_17_0_2_4_b9202e7_1402410477887_979887_2190"
      property "last_name", :type => "UML::String", :id => "_17_0_2_4_b9202e7_1402410490650_559652_2192"
    end
    klass "Perspective", :id => "_16_9_78e0236_1361999978172_908613_1658" do
      property nil, :type => "Gui_Builder_Profile::InvitationCode", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000053868_346349_1709"
      property nil, :type => "Gui_Builder_Profile::User", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000016909_933287_1689"
      property "name", :type => "UML::String", :id => "_16_9_78e0236_1362000125819_873673_1725" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "substitutions", :type => "Gui_Builder_Profile::MultivocabularySubstitution", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1361999994973_454878_1677"
    end
    klass "ProjectOptions", :id => "_16_9_78e0236_1361379064628_70918_1919" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::singleton"
      property "email_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1362435833929_936829_1540"
      property "password_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1362435861830_763541_1549"
      property "user_registration", :type => "Gui_Builder_Profile::UserRegistrationType", :default_value => "Open", :id => "_16_9_78e0236_1361379249201_791722_1950"
    end
    klass "RegularExpressionCondition", :parents => ["Gui_Builder_Profile::StringCondition"], :id => "_16_9_78e0236_1362435709826_262438_1513" do
      property "regular_expression", :type => "Gui_Builder_Profile::RegularExpression", :id => "_16_9_78e0236_1362435738776_707179_1516" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
    end
    klass "RichText", :id => "_17_0_4_2_78e0236_1386346294425_836962_3291" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "content", :type => "UML::String", :id => "_17_0_4_2_78e0236_1386346294426_434823_3293" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "images", :type => "Gui_Builder_Profile::RichTextImage", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_4_2_78e0236_1386346294428_392424_3297"
      property "markup_language", :type => "Gui_Builder_Profile::MarkupType", :id => "_17_0_4_2_78e0236_1386346294427_626186_3294"
    end
    klass "RichTextImage", :id => "_17_0_4_2_78e0236_1386346294425_790237_3292" do
      property nil, :type => "Gui_Builder_Profile::RichText", :id => "_17_0_4_2_78e0236_1386346294429_948280_3299"
      property "image", :type => "Gui_Builder_Profile::File", :id => "_17_0_4_2_78e0236_1386346294427_443952_3295"
    end
    klass "RolePermissions", :id => "_16_9_78e0236_1361377547806_696994_1711" do
      property "role_manager", :type => "UML::Boolean", :id => "_16_9_78e0236_1361377597251_847958_1743"
    end
    klass "StringCondition", :id => "_16_9_78e0236_1362435638076_723054_1493", :is_abstract => true do
      property "additional_email_requirement_for", :type => "Gui_Builder_Profile::InvitationCode", :id => "_16_9_78e0236_1362444527652_678514_1496"
      property "description", :type => "UML::String", :id => "_16_9_78e0236_1362439385411_917299_1696"
      property "email_requirement_for", :type => "Gui_Builder_Profile::ProjectOptions", :id => "_16_9_78e0236_1362435833929_239766_1541"
      property "failure_message", :type => "UML::String", :id => "_16_9_78e0236_1362435763496_769708_1518"
      property "password_requirement_for", :type => "Gui_Builder_Profile::ProjectOptions", :id => "_16_9_78e0236_1362435861830_154310_1550"
    end
    klass "User", :parents => ["Gui_Builder_Profile::Person"], :id => "_16_9_6620216_1311349088211_41010_1823" do
      property nil, :type => "Gui_Builder_Profile::Perspective", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000016908_686909_1688"
      property "login", :type => "UML::String", :id => "_16_9_6620216_1311349134111_355250_1824" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "password_hash", :type => "UML::String", :id => "_16_9_6620216_1311349178120_340215_1825"
      property "roles", :type => "Gui_Builder_Profile::UserRole", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361377541576_508361_1703"
      property "salt", :type => "UML::String", :id => "_16_9_6620216_1311349196390_496663_1826"
      property "use_accessibility", :type => "UML::Boolean", :id => "_16_9_6620216_1311349222322_790429_1827"
    end
    klass "UserRole", :id => "_16_9_6620216_1311349580039_47554_1831" do
      property "invitations", :type => "Gui_Builder_Profile::InvitationCode", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361654888054_232347_1462"
      property "name", :type => "UML::String", :id => "_16_9_6620216_1311349591342_46442_1832" do
        applied_stereotype :instance_of => "Gui_Builder_Profile::descriptor"
      end
      property "registration", :type => "Gui_Builder_Profile::RoleRegistrationType", :default_value => "Role Manager", :id => "_16_9_78e0236_1361377688965_687613_1769"
      property "users", :type => "Gui_Builder_Profile::User", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361377541576_939654_1704"
    end
    enumeration "FacadeImplementationLanguages", :id => "_16_8_6620216_1283178501783_51776_1249" do
      property "value", :type => "UML::String"
      literal "Java", :id => "_16_8_6620216_1283178557628_106569_1250"
      literal "Ruby", :id => "_16_8_6620216_1283178566324_155772_1251"
    end
    enumeration "LanguageType", :id => "_16_9_28b0142_1305737723715_669375_1467" do
      property "value", :type => "UML::String"
      literal "Clojure", :id => "_16_9_28b0142_1305737723716_562372_1470"
      literal "Groovy", :id => "_16_9_28b0142_1305737723716_180112_1471"
      literal "Javascript", :id => "_16_9_28b0142_1305737723716_116462_1469"
      literal "Python", :id => "_16_9_28b0142_1305737723717_525399_1472"
      literal "Ruby", :id => "_16_9_28b0142_1305737723715_442867_1468"
    end
    enumeration "MarkupType", :id => "_17_0_4_2_78e0236_1386346253949_528969_3262" do
      property "value", :type => "UML::String"
      literal "HTML", :id => "_17_0_4_2_78e0236_1386346253951_61218_3266"
      literal "LaTeX", :id => "_17_0_4_2_78e0236_1386346253951_848506_3264"
      literal "Markdown", :id => "_17_0_4_2_78e0236_1386346253951_125387_3263"
      literal "Plain", :id => "_17_0_4_2_78e0236_1386346253951_470997_3265"
      literal "Textile", :id => "_17_0_4_2_78e0236_1386346253951_18150_3267"
    end
    enumeration "RoleRegistrationType", :id => "_16_9_78e0236_1361377755013_916086_1790" do
      property "value", :type => "UML::String"
      literal "Administrator", :id => "_16_9_78e0236_1361377828577_385709_1816"
      literal "Open", :id => "_16_9_78e0236_1361377757085_493086_1808"
      literal "Role", :id => "_16_9_78e0236_1361377761232_512485_1810"
      literal "Role Manager", :id => "_16_9_78e0236_1361377824614_582502_1814"
    end
    enumeration "UserRegistrationType", :id => "_16_9_78e0236_1361379303537_735379_1975" do
      property "value", :type => "UML::String"
      literal "Email", :id => "_16_9_78e0236_1361379315080_235305_1995"
      literal "Invitation", :id => "_16_9_78e0236_1361379375691_302758_1997"
      literal "Open", :id => "_16_9_78e0236_1361379305457_933520_1993"
    end
    package "OperationTypes", :id => "_16_6_2_28b0142_1276094884677_210_353" do
      stereotype "create", :metaclasses => ["Element"], :id => "_16_6_2_28b0142_1276094884695_149456_364"
      stereotype "delete", :metaclasses => ["Element"], :id => "_16_6_2_28b0142_1276094884696_634303_368"
      stereotype "other", :metaclasses => ["Element"], :id => "_16_6_2_28b0142_1276094884696_548790_372"
      stereotype "retrieve", :metaclasses => ["Element"], :id => "_16_6_2_28b0142_1276094884696_299089_370"
      stereotype "update", :metaclasses => ["Element"], :id => "_16_6_2_28b0142_1276094884695_884805_366"
    end
    package "Spec", :id => "_16_9_28b0142_1348843187845_26689_1563" do
      klass "SpecButton", :id => "_16_9_28b0142_1348843906738_927409_2238" do
        property "action", :type => "UML::String", :id => "_16_9_28b0142_1348844123737_930520_2239", :documentation => "This attribute ('action') is the name of the method called on the object when the button is clicked."
        property "button_text", :type => "UML::String", :id => "_16_9_28b0142_1348844299361_590793_2243", :documentation => "This is the text that should appear on the button"
        property "display_result", :type => "Gui_Builder_Profile::Spec::SpecButtonResultType", :default_value => "none", :id => "_16_9_28b0142_1348844602297_894974_2254"
        property "image_actions", :type => "UML::String", :upper => Float::INFINITY, :id => "_16_9_28b0142_1348844863936_421596_2256", :documentation => "If producing a web page or a popup, these are callbacks to methods on the object that produce an image."
        property "label", :type => "UML::String", :id => "_16_9_28b0142_1348844209497_532218_2241", :documentation => "This is the content for the label associated with the button."
        property "save_page", :type => "UML::Boolean", :default_value => true, :id => "_16_9_28b0142_1348844335673_668740_2245", :documentation => "Whether or not clicking the button should save the current page.\nSetting to false is not currently supported."
      end
      klass "SpecLabel", :id => "_16_9_28b0142_1348843791569_572995_2236" do
        property "label_content", :type => "UML::String", :id => "_16_9_28b0142_1348843835969_790479_2237"
      end
      enumeration "SpecButtonResultType", :id => "_16_9_28b0142_1348844447336_451312_2249" do
        property "value", :type => "UML::String"
        literal "file", :id => "_16_9_28b0142_1348844485281_618841_2251"
        literal "none", :id => "_16_9_28b0142_1348844576441_890323_2253"
        literal "popup", :id => "_16_9_28b0142_1348844466969_78735_2250"
        literal "web_page", :id => "_16_9_28b0142_1348844526881_142221_2252"
      end
      enumeration "SpecSearchFilterType", :id => "_16_9_28b0142_1348845268810_715236_2272" do
        property "value", :type => "UML::String"
        literal "boolean", :id => "_16_9_28b0142_1348845355785_991128_2276"
        literal "case_insensitive_exact", :id => "_16_9_28b0142_1348845400673_105761_2278"
        literal "case_insensitive_like", :id => "_16_9_28b0142_1348845449641_876422_2279"
        literal "case_sensitive_exact", :id => "_16_9_28b0142_1348845380976_374802_2277"
        literal "chemical_system", :id => "_16_9_28b0142_1348845299696_392194_2275"
        literal "default", :id => "_16_9_28b0142_1348845295489_940153_2274"
        literal "disabled", :id => "_16_9_28b0142_1348845280626_522872_2273"
      end
      stereotype "disabled", :metaclasses => ["Property"], :id => "_16_9_28b0142_1348843386274_821938_1935"
      stereotype "expanded", :metaclasses => ["Property"], :id => "_16_9_28b0142_1348843675771_373439_2214"
      stereotype "hidden", :metaclasses => ["Property"], :id => "_16_9_28b0142_1348843197786_542705_1564"
      stereotype "label", :metaclasses => ["Element"], :id => "_16_9_28b0142_1348843570538_356246_2207" do
        tag "label_name", :id => "_16_9_28b0142_1348843607345_515355_2213"
      end
      stereotype "search_filter", :metaclasses => ["Property"], :id => "_16_9_28b0142_1348845065376_563917_2260" do
        tag "search_filter", :id => "_16_9_28b0142_1348845531953_956461_2280"
      end
      stereotype "show_possible", :metaclasses => ["Property"], :id => "_16_9_28b0142_1348843746490_604058_2225"
    end
    primitive "RegularExpression", :parents => ["UML::String"], :id => "_16_9_78e0236_1361379717284_94123_2076"
    primitive "Timestamp", :id => "_16_9_28b0142_1305737647128_481773_1466"
    stereotype "acts_as_user", :metaclasses => ["Class", "Interface"], :id => "_16_9_28b0142_1320260337354_999263_1561"
    stereotype "alt_root", :metaclasses => ["Class"], :id => "_16_6_2_28b0142_1276094884674_683422_345"
    stereotype "change_tracked", :metaclasses => ["Package"], :id => "_16_9_28b0142_1320261650047_288289_1577"
    stereotype "complex_attribute", :metaclasses => ["Class"], :id => "_16_9_28b0142_1350587904204_746275_1536"
    stereotype "descriptor", :metaclasses => ["Property"], :id => "_16_9_78e0236_1364996481824_468367_1850"
    stereotype "facade", :metaclasses => ["Interface"], :id => "_16_6_2_28b0142_1276094884676_922858_351"
    stereotype "facade_method", :metaclasses => ["Package"], :id => "_16_8_44701b5_1282141440769_95001_1139" do
      tag "implementation", :id => "_16_8_44701b5_1282141649857_750585_1150"
      tag "language", :id => "_16_8_6620216_1283178454902_25253_1248"
    end
    stereotype "nil_for_root", :metaclasses => ["Property"], :id => "_16_6_2_28b0142_1276094884675_838651_347", :documentation => "When specified on a property or association of a <<root>> Class, causes any Class of that type to be considered non-root if the <<nil_for_root>> association or property is set."
    stereotype "options", :metaclasses => ["Package"], :id => "_16_9_28b0142_1284660904944_537040_1463" do
      tag "require", :id => "_16_9_28b0142_1284660999929_840326_1469"
      tag "version", :id => "_16_9_6620216_1311350018593_840420_1847"
      tag "project_name", :id => "_16_9_78e0236_1366130322652_436960_1623"
    end
    stereotype "root", :metaclasses => ["Class"], :id => "_16_6_2_28b0142_1276094884676_130193_349" do
      tag "Root Name", :id => "_16_6_2_28b0142_1276094884694_127513_360"
    end
    stereotype "singleton", :metaclasses => ["Class"], :id => "_16_9_78e0236_1361379210582_818602_1938"
  end
  profile "XML Schema", :id => "_17_0_2_4_78e0236_1389733516832_463470_3756" do
    applied_stereotype :instance_of => "XML Schema::XML Schema"
    stereotype "Root", :metaclasses => ["Class"], :id => "_17_0_2_4_78e0236_1389733516835_503019_3761"
    stereotype "XML Schema", :metaclasses => ["Package"], :id => "_17_0_2_4_78e0236_1389733516833_591313_3757" do
      tag "targetNamespace", :id => "_17_0_2_4_78e0236_1389733516836_616946_3765"
      tag "prefix", :id => "_17_0_2_4_78e0236_1389733516837_63052_3767"
      tag "use_collection_containers", :id => "_17_0_2_4_f2e0365_1408546004129_112220_2456"
      tag "only_keep_compositions", :id => "_18_0_2_6340208_1497878482190_161398_4579"
      tag "always_keep_compositions", :id => "_17_0_2_4_f2e0365_1408546053775_998333_2457"
      tag "object_id_only_if_referenced", :id => "_18_0_2_6340208_1426877181082_777189_4521"
      tag "append_id_to_reference_names", :id => "_18_0_2_6340208_1427145213757_729903_4578"
      tag "id_attribute_name", :id => "_18_0_2_6340208_1428334123902_822527_4404"
      tag "enforce_case_conventions", :id => "_18_0_2_6340208_1428334233366_387585_4406"
      tag "append_type_to_type_names", :id => "_18_0_2_6340208_1428349794236_589100_4406"
      tag "uppercase_attribute_names", :id => "_18_0_2_6340208_1428349871312_946791_4410"
      tag "include_documentation_annotations", :id => "_17_0_2_4_f71035d_1429111542006_612446_2220"
      tag "documentation_inline", :id => "_17_0_2_4_f71035d_1429112217095_733972_2491"
      tag "id_suffix_for_references", :id => "_17_0_2_4_f71035d_1429112264773_570667_2492"
      tag "full_version", :id => "_18_0_2_6340208_1429712540104_396161_4581"
      tag "schema_filename", :id => "_18_0_2_6340208_1429712596915_596636_4583"
      tag "major_version", :id => "_18_0_2_6340208_1429712627561_929961_4584"
      tag "use_idrefs", :id => "_18_0_2_6340208_1492794748860_157562_4542"
      tag "id_suffix_for_plural_references", :id => "_18_0_2_6340208_1492794815233_760674_4544"
      tag "generate_attributes_as_elements", :id => "_18_0_2_6340208_1492794995681_715546_4557"
      tag "sort_other_attributes_following_base_attributes", :id => "_18_0_2_6340208_1497637212679_545931_4553"
      tag "sort_end_attributes_following_start_attributes", :id => "_18_0_2_6340208_1497881236557_420094_4556"
      tag "beginning_comment", :id => "_18_0_2_6340208_1497883307115_695890_4561"
    end
    stereotype "extendable", :metaclasses => ["Interface"], :id => "_17_0_2_4_78e0236_1389733516836_571937_3762"
    stereotype "referenceElement", :metaclasses => ["Property"], :id => "_18_0_2_6340208_1429558036376_377346_4547"
    stereotype "simpleContent", :metaclasses => ["Property"], :id => "_18_0_2_6340208_1429122553128_724807_4416"
    stereotype "xmlAttribute", :metaclasses => ["Property"], :id => "_18_0_2_6340208_1505851809798_81479_4560"
    stereotype "xmlElement", :metaclasses => ["Property"], :id => "_18_0_2_6340208_1441315606468_451262_4583"
  end
end
