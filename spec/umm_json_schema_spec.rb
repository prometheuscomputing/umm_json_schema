require 'spec_helper'
require 'json-schema'

# Election Results Reporting tests
describe 'JSON Schema translation of UML Metamodel using serialized ERR model' do
  # This :all block is run once before all of the tests in this 'describe' block
  before :all do
    err_umm_dsl_file = File.join(TEST_DATA, 'ERR-V2.0.rb')
    @schema = UmmJsonSchema.generate_from_dsl_file(err_umm_dsl_file, :references => :id_attributes)
    # Output entire generated JSON Schema for ERR to file (tmp/ERR.json)
    File.open(File.join(TMP_DIR, 'ERR.json'), 'w+'){|f| f.puts @schema }
    # First character in schema must be a '{'
    @schema.should =~ /\A\{/
    # Parse JSON Schema using Ruby's JSON library
    @json = JSON.parse(@schema)
    @json['$schema'].should == 'http://json-schema.org/draft-04/schema#'
    example_path = File.join(TEST_DATA, 'example.json')
    @example = JSON.parse(File.read(example_path))
  end

  # Test the basic properties of the schema
  it 'should generate a valid JSON schema that validates example.json and la_county_reference.json' do
    #@json['id'].should == 'http://www.prometheuscomputing.com/ERR.json'
    @json['definitions'].should be_a(Hash)
    result = JSON::Validator.fully_validate(@json, @example)
    result.should == []
    la_county_json = File.read(File.join(TEST_DATA, 'la_county_reference.json'))
    result = JSON::Validator.fully_validate(@json, la_county_json)
    result.should == []
  end
  
  it 'should not generate definitions for abstract classes' do
    @json['definitions'].keys.should_not include('ElectionResults.Contest')
    @json['definitions'].keys.should_not include('ElectionResults.BallotSelection')
    @json['definitions'].keys.should_not include('ElectionResults.Counts')
    @json['definitions'].keys.should_not include('ElectionResults.GpUnit')
  end

  # Test that the ElectionReport->Office association is correctly represented in JSON Schema
  it 'should generate a correct definition for a 0..* association' do
    # Test that the ElectionReport definition has a property named 'office'
    @json['definitions']['ElectionResults.ElectionReport']['properties'].keys.should include('Office')

    # Test that the ElectionReport definition's property 'office' is defined as an array where every element is an Office
    @json['definitions']['ElectionResults.ElectionReport']['properties']['Office']['type'].should == 'array'
    @json['definitions']['ElectionResults.ElectionReport']['properties']['Office']['items']['$ref'].should == '#/definitions/ElectionResults.Office'
    # Note that the above check could also be expressed as follows:
    er_offices_prop = @json['definitions']['ElectionResults.ElectionReport']['properties']['Office']
    er_offices_prop['type'].should == 'array'
    er_offices_prop['items']['$ref'].should == '#/definitions/ElectionResults.Office'
  end

  it 'should correctly specify which attributes are required' do
    @json['definitions']['ElectionResults.ElectionReport']['required'].should =~ ['GeneratedDate', 'Format', 'Issuer', 'IssuerAbbreviation', 'SequenceStart', 'SequenceEnd', 'Status', 'VendorApplicationId', '@type']
    @json['definitions']['ElectionResults.ElectionReport']['additionalProperties'].should == false
  end

  it 'should correctly translate XML primitives described via "Primitives (Class based)"' do
    # These primitives should get converted to UML primitives and the package should be removed when initially parsing UMM file.
    @schema.should_not =~ /Primitives (Class based)/
    @json['definitions']['ElectionResults.ElectionReport']['properties']['SequenceStart']['type'].should == 'integer'
  end

  it 'should only add required associations and attributes to the "required" array' do
    # Test required [1..*] reference assoc
    @json['definitions']['ElectionResults.PartySelection']['required'].should be_an(Array)
    expect(@json['definitions']['ElectionResults.PartySelection']['required']).to include('PartyIds')
    # Test optional [0..1] reference assoc (Person -> Party should not be required)
    @json['definitions']['ElectionResults.Person']['required'].should =~ ['@id', '@type']
    # Test optional [0..*] composition assoc
    expect(@json['definitions']['ElectionResults.PartySelection']['required']).not_to include('VoteCounts')
    # Test required [1] assoc
    expect(@json['definitions']['ElectionResults.Election']['required']).to include('ElectionScopeId')
    # Test required [1] attribute
    expect(@json['definitions']['ElectionResults.Election']['required']).to include('StartDate')
  end

  it 'should not specify that an element should be "oneOf" a set of definitions if there is only one choice' do
    # Test Root element (must be ElectionResults.ElectionReport)
    @json['$ref'].should == "#/definitions/ElectionResults.ElectionReport"
    @json['oneOf'].should == nil
  end

  it 'should correctly represent inheritance when defining association choices' do
    # Test ElectionReport -> Party (should allow Party and Coalition definitions)
    def_choice = @json['definitions']['ElectionResults.ElectionReport']['properties']['Party']['items']['oneOf']
    def_choice.should be_an(Array)
    choices = def_choice.collect{|h| h['$ref']}
    choices.should =~ ['#/definitions/ElectionResults.Party', '#/definitions/ElectionResults.Coalition']
  end

  it 'should only interpet non-composition associations as references' do
    #Test Coalition -> Party [0..*] non-composition association
    coalition_parties = @json['definitions']['ElectionResults.Coalition']['properties']['PartyIds']
    coalition_parties.keys.should =~ ['items', 'minItems', 'type']
    coalition_parties['type'].should == 'array'
    coalition_parties['minItems'].should == 0
    items = coalition_parties['items']
    items.keys.should =~ ['type', 'refTypes']
    items['type'].should == 'string'
  end

  it 'should correctly interpret classes inheriting from primitives with appropriate constraints' do
    # Note: Interpret the 'pattern' property of UML string primitives as a special property that
    #         defines a regular expression which the primitive must conform to.
    #         Additionally, the default_value of this property should be used as the value of that regex.
    # Note: HtmlColorString and TimeWithZone both initially inherit from primitives in Primitives (Class based)::xsd,
    #         but this is altered by the call to UmlMetamodel.fix_project_elements! to inherit from UML primitives.
    #       After the call, they should inherit from UmlMetamodel::PRIMITIVES[:string] and
    #         UmlMetamodel::PRIMITIVES[:time]
    # Test HtmlColorString
    @json['definitions']['ElectionResults.HtmlColorString'].keys.should =~ ['type', 'pattern']
    @json['definitions']['ElectionResults.HtmlColorString']['type'].should == 'string'
    @json['definitions']['ElectionResults.HtmlColorString']['pattern'].should == '[0-9a-f]{6}'
    # Test TimeWithZone
    @json['definitions']['ElectionResults.TimeWithZone'].keys.should =~ ['type', 'pattern', 'format']
    @json['definitions']['ElectionResults.TimeWithZone']['type'].should == 'string'
    @json['definitions']['ElectionResults.TimeWithZone']['pattern'].should == '(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]|(24:00:00))(Z|[+-]((0[0-9]|1[0-3]):[0-5][0-9]|14:00))'
    @json['definitions']['ElectionResults.TimeWithZone']['format'].should == 'time'
  end
end


# Election Results Reporting tests
describe 'JSON Schema translation of UML Metamodel using serialized ERR model (alternate :ref_objects references)' do
  # This :all block is run once before all of the tests in this 'describe' block
  before :all do
    err_umm_dsl_file = File.join(TEST_DATA, 'ERR-V2.0.rb')
    @schema = UmmJsonSchema.generate_from_dsl_file(err_umm_dsl_file)
    # First character in schema must be a '{'
    @schema.should =~ /\A\{/
    # Parse JSON Schema using Ruby's JSON library
    @json = JSON.parse(@schema)
    @json['$schema'].should == 'http://json-schema.org/draft-04/schema#'
  end
  
  # :references => :ref_objects ERR test
  it 'should only interpet non-composition associations as references' do
    #Test Coalition -> Party [0..*] non-composition association
    coalition_parties = @json['definitions']['ElectionResults.Coalition']['properties']['Party']
    coalition_parties.keys.should =~ ['items', 'minItems', 'type']
    coalition_parties['items'].keys.should =~ ['additionalProperties', 'properties', 'type', 'refTypes', 'required']
    coalition_parties['items']['required'].should =~ ['@ref']
    coalition_parties['items']['properties'].keys.should =~ ['@ref']
    #Test CandidateContest -> Party [0..1] non-composition association
    # NOTE: this association has changed its multiplicity, and may change again, so I'm commenting out this part of the test
    # contest_party = @json['definitions']['ElectionResults.CandidateContest']['properties']['PrimaryParty']
    # contest_party.keys.should =~ ['additionalProperties', 'properties', 'type', 'refTypes']
    # contest_party['properties'].keys.should =~ ['@ref']
    # Test RetentionContest -> Office [0..1] non-composition association
    retention_contest_office = @json['definitions']['ElectionResults.RetentionContest']['properties']['Office']
    retention_contest_office.keys.should =~ ['additionalProperties', 'properties', 'type', 'refTypes', 'required']
    retention_contest_office['properties'].keys.should =~ ['@ref']
    # Test Office -> Term [0..1] composition association
    office_term = @json['definitions']['ElectionResults.Office']['properties']['Term']
    office_term.keys.should =~ ['$ref']
    office_term['$ref'] = "#/definitions/ElectionResults.Term"
    # Test CandidateContest -> BallotSelection [0..*] composition association
    contest_selections = @json['definitions']['ElectionResults.CandidateContest']['properties']['BallotSelection']
    contest_selections.keys.should =~ ['items', 'minItems', 'type']
    contest_selections['items'].keys.should =~ ['oneOf']
    selection_choices = contest_selections['items']['oneOf'].collect{|h| h['$ref'].gsub(/^#\/definitions\//, '')}
    selection_choices.should =~ ['ElectionResults.CandidateSelection', 'ElectionResults.PartySelection', 'ElectionResults.BallotMeasureSelection']
  end
end

# CarExample tests
describe 'JSON Schema translation of UML Metamodel using serialized CarExample model' do
  # This :all block is run once before all of the tests in this 'describe' block
  before :all do
    car_example_umm_dsl_file = File.join(TEST_DATA, 'CarExample.rb')
    @schema = UmmJsonSchema.generate_from_dsl_file(car_example_umm_dsl_file)
    File.open(File.join(TMP_DIR, 'CAR.json'), 'w+'){|f| f.puts @schema }
    # First character in schema must be a '{'
    @schema.should =~ /\A\{/
    # Parse JSON Schema using Ruby's JSON library
    @json = JSON.parse(@schema)
    @json['$schema'].should == 'http://json-schema.org/draft-04/schema#'
  end

  it 'should generate a valid json schema for the given CarExample Metamodel' do
    #@json['id'].should == 'http://www.prometheuscomputing.com/Application.json'
    @json['definitions'].should be_a(Hash)
  end

  it 'should specify all possible root elements as a "oneOf" choice in the schema' do
    @json['$ref'].should == nil
    @json['oneOf'].should be_an(Array)
    root_classes = @json['oneOf'].collect{|h| h['$ref'].gsub(/^#\/definitions\//, '')}
    root_classes.should =~ ['Automotive.Component', 'Automotive.RepairShop', 'Automotive.VIN',
      'Automotive.Car', 'Automotive.ElectricVehicle', 'Automotive.HybridVehicle', 'Automotive.Minivan', 'Automotive.Motorcycle', 'People.Clown.Unicycle',
      'Automotive.Warranty', 'Geography.Country', 'People.Person', 'Automotive.Mechanic', 'People.Clown.Clown']
  end
  
  it 'should interpet non-composition associations as references' do
    #Test Car -> Occupants [0..*] non-composition association
    vehicle_occupants = @json['definitions']['Automotive.Car']['properties']['occupants']
    vehicle_occupants.keys.should =~ ['items', 'minItems', 'type']
    vehicle_occupants['items'].keys.should =~ ['additionalProperties', 'properties', 'type', 'refTypes', 'required']
    vehicle_occupants['items']['required'].should =~ ['@ref']
    vehicle_occupants['items']['properties'].keys.should =~ ['@ref']
  end
end