require 'rspec'
require 'simplecov'
require 'fileutils'
require 'json'

# Enable old syntax for Rspec 3.0
RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end

SimpleCov.start do
  add_filter 'spec/'
  add_filter 'tmp/'
end

# Create tmp directory for testing
TMP_DIR = File.expand_path('../../tmp', __FILE__)
FileUtils.mkdir(TMP_DIR) unless File.exists?(TMP_DIR)

# Set TEST_DATA constant
TEST_DATA = File.expand_path('../../test_data', __FILE__)

# Put local json_graph on the load path
# $:.unshift(File.expand_path('../../../json_graph/lib/',__FILE__))
# Put local uml_metamodel on the load path
$:.unshift(File.expand_path('../../../uml_metamodel/lib/',__FILE__))

require_relative '../lib/umm_json_schema'