# MagicDraw 18.5 XML/JSON Schema Plugins Documentation
These instructions cover the installation and usage of Prometheus' MagicDraw plugins (for MagicDraw 18.5) that generate XML and JSON schemas from UML class diagrams.

## Installation (Ubuntu 20.04)
- Install MagicDraw 18.5
  - Follow No Magic's installation instructions to download and install MagicDraw 18.5
- Install and configure RVM
  - In Terminal, run:
  ```bash
  sudo apt-add-repository -y ppa:rael-gc/rvm
  sudo apt-get update
  sudo apt-get install rvm
  sudo usermod -a -G rvm $USER
  ```
  - Set terminal to run command as a login shell
[![Terminal Screenshot](https://github.com/rvm/ubuntu_rvm/raw/master/terminal.png)](https://github.com/rvm/ubuntu_rvm/blob/master/terminal.png)
  - Restart the computer (logout is *not* sufficient)
  - After restart and login, continue setup:
  ```bash
  rvm autolibs enable
  gem sources --add 'https://pro-md-185:!Ep!meTh3us!@gems.prometheuscomputing.com/'
  ```
- Install and configure JRuby 9.2.0.0 & install gems
  ```bash
  rvm install jruby-9.2.0.0
  rvm use jruby-9.2.0.0
  gem install umm_json_schema
  rvm gemset create MagicDraw
  rvm gemset use MagicDraw
  gem install
  gem install plugin.schemaGen
  gem install plugin.modelGen
  ```
- Install RubyAdapter MagicDraw plugin
  - Download https://gitlab.com/prometheuscomputing/plugin.rubyadapter/uploads/271b98a0b51d34635763dc579cba0488/com.prometheus.rubyAdapter.zip
    - Use the download button in the upper right corner to download the entire zip
  - Extract to the 'plugins' folder of your MagicDraw directory (For me: ~/MagicDraw Personal Edition/plugins)
- Hack MagicDraw's automaton plugin to use a modern and compatible version of JRuby
  - Download https://repo1.maven.org/maven2/org/jruby/jruby-complete/9.2.0.0/jruby-complete-9.2.0.0.jar
  - Rename existing Automaton JRuby Complete Jar and replace with JRuby 9.2.0.0 Complete Jar
  ```bash
  mv ~/MagicDraw\ Personal\ Edition/plugins/com.nomagic.magicdraw.automaton/engines/jruby-complete-1.7.26.jar ~/MagicDraw\ Personal\ Edition/plugins/com.nomagic.magicdraw.automaton/engines/jruby-complete-1.7.26.jar.original
  mv ~/Downloads/jruby-complete-9.2.0.0.jar ~/MagicDraw\ Personal\ Edition/plugins/com.nomagic.magicdraw.automaton/engines/jruby-complete-1.7.26.jar
  ```
- Install Gui Builder Profiles
  - Download https://gitlab.com/prometheuscomputing/plugin.rubyadapter/uploads/f393a0c13924d18bb6331f0b7e8e356a/GUI_Builder_Profiles.zip
  - Extract files to the 'profiles' directory of your MagicDraw directory (For me: ~/MagicDraw Personal Edition/profiles)

## Usage

### XML Schema generation
- From MagicDraw in the containment browser, right-click on the package you wish to generate a schema for (*not* the Data package). It will be the one with the \<\<XML Schema\>\> stereotype applied that isn’t imported by other packages. For the VRI CDF, this is the "VRI" package.  
- Select "Generate XML Schema"
- Check the ~/Prometheus/Generated_Code folder for the resulting XML Schema

### JSON Schema generation
- From MagicDraw in the containment browser, right-click the Data package.
- Select "Generate Ruby UML Metamodel"
  - You should get a message that the translation succeeded. 
- A UMM file (.rb extension) will be generated in the ~/Prometheus/Generated_Code folder
- Run umm_json_schema on the UMM file
  ```bash
  umm_json_schema <path-to-UMM-file> -o <output-filename> -r id_attributes
  ``` 
- Check the current directory for the resulting JSON Schema (it will be named whatever you filled in for \<output-filename\> above.
