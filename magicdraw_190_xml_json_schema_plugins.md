# MagicDraw 19 SP4 XML/JSON Schema Plugins Documentation
These instructions cover the installation and usage of Prometheus' MagicDraw plugins (for MagicDraw 19.0) that generate XML and JSON schemas from UML class diagrams.

## Installation (Xubuntu 20.04)
- Install essential packages
  - In Terminal, run:
  ```bash
  sudo apt install build-essential dkms linux-headers-$(uname -r)
  ```
- (For VirtualBox VM only) Install Guest Additions
  - Insert Guest Additions CD Image
  - Navigate to mounted image
  - `` sudo VBoxLinuxAdditions.run``
  - Reboot
- Update software (optional)
  ```bash
  sudo apt update # Fetches the list of available updates
  sudo apt --assume-yes upgrade # Installs some updates; does not remove packages
  sudo apt --assume-yes full-upgrade # Installs updates; may also remove some packages, if needed
  sudo apt --assume-yes autoremove # Removes any old packages that are no longer needed
  ```
- Install and configure RVM
  ```bash
  sudo apt-add-repository -y ppa:rael-gc/rvm
  sudo apt update
  sudo apt install rvm
  sudo usermod -a -G rvm $USER
  ```
  - Set terminal to run command as a login shell
[![Terminal Screenshot](https://github.com/rvm/ubuntu_rvm/raw/master/terminal.png)](https://github.com/rvm/ubuntu_rvm/blob/master/terminal.png)
  - Restart the computer (logout is *not* sufficient)
  - After restart and login, continue setup:
  ```bash
  rvm autolibs enable
  ```
- Symlink /usr/share/rvm to ~/.rvm
  - This is a hack to avoid setting an env variable to point to the correct RVM location for the Prometheus MagicDraw plugins
  ```bash
   ln -s /usr/share/rvm/ ~/.rvm
   ```
- Install and configure JRuby 9.2.0.0 & install gems
  ```bash
  rvm install jruby-9.2.0.0
  rvm use jruby-9.2.0.0
  gem sources --add 'https://pro-md-19:!Ep!meTh3us!@gems.prometheuscomputing.com/'
  gem install umm_json_schema
  rvm gemset create MagicDraw
  rvm gemset use MagicDraw
  gem install plugin.schemaGen plugin.modelGen
  ```
- Install Arial font
  ```bash
  sudo apt install ttf-mscorefonts-installer
  sudo fc-cache -f
  ```
- Disable Java VM gnome-dependent assistive technology requirement
  - Not required for regular (non-Xfce) Ubuntu
  ```bash
  sudo sed -i -e '/^assistive_technologies=/s/^/#/' /etc/java-*-  openjdk/accessibility.properties
  ```
- Install MagicDraw 19.0
  - Follow No Magic's installation instructions to download, install, and activate MagicDraw 19.0
- Update MagicDraw to 19.0 SP3 (NOTE: may be unnecessary due to installing SP4)
  - From MagicDraw, select Help -> Check for Updates and follow prompts
- Update MagicDraw to 19.0 SP4
	- Download https://drive.google.com/file/d/16iwJhSerHog6bY0ezp0k_BjwJYW8__uy/view?usp=sharing
		- I'm providing the SP4 update via Google Drive since the automatic updater can't seem to find it. MagicDraw provides a [cloudfront URL](https://d1oqhepk9od1tu.cloudfront.net/190sp4/sp4_MagicDraw_190.zip) for downloading the update, but it may be temporary.
	- Extract to your MagicDraw folder (for me: ~/MagicDraw), overwriting files.
- Install RubyAdapter MagicDraw plugin
  - Download https://gitlab.com/prometheuscomputing/plugin.rubyadapter/uploads/271b98a0b51d34635763dc579cba0488/com.prometheus.rubyAdapter.zip
    - Use the download button in the upper right corner to download the entire zip
  - Extract to the 'plugins' folder of your MagicDraw directory (For me: ~/MagicDraw/plugins)
- Hack MagicDraw's automaton plugin to use a modern and compatible version of JRuby
  - Download https://repo1.maven.org/maven2/org/jruby/jruby-complete/9.2.0.0/jruby-complete-9.2.0.0.jar
  - Rename existing Automaton JRuby Complete Jar and replace with JRuby 9.2.0.0 Complete Jar
  ```bash
  mv ~/MagicDraw/plugins/com.nomagic.magicdraw.automaton/engines/jruby-complete-1.7.26.jar ~/MagicDraw/plugins/com.nomagic.magicdraw.automaton/engines/jruby-complete-1.7.26.jar.original
  cp ~/Downloads/jruby-complete-9.2.0.0.jar ~/MagicDraw/plugins/com.nomagic.magicdraw.automaton/engines/jruby-complete-1.7.26.jar
  ```
  - After installing 19.0 SP3/SP4, there seems to be another copy of jruby-complete inside the plugin's lib dir, so replace that one as well
  ```bash
  mv ~/MagicDraw/plugins/com.nomagic.magicdraw.automaton/lib/engines/jruby-complete-1.7.26.jar ~/MagicDraw/plugins/com.nomagic.magicdraw.automaton/lib/engines/jruby-complete-1.7.26.jar.original
  cp ~/Downloads/jruby-complete-9.2.0.0.jar ~/MagicDraw/plugins/com.nomagic.magicdraw.automaton/lib/engines/jruby-complete-1.7.26.jar
  ```
- Install Gui Builder Profiles
  - Download https://gitlab.com/prometheuscomputing/plugin.rubyadapter/uploads/f393a0c13924d18bb6331f0b7e8e356a/GUI_Builder_Profiles.zip
  - Extract files to the 'profiles' directory of your MagicDraw directory (For me: ~/MagicDraw/profiles)

## Usage

### XML Schema generation
- From MagicDraw in the containment browser, right-click on the package you wish to generate a schema for (*not* the Data package). It will be the one with the \<\<XML Schema\>\> stereotype applied that isn’t imported by other packages. For the VRI CDF, this is the "VRI" package.  
- Select "Generate XML Schema"
- Check the ~/Prometheus/Generated_Code folder for the resulting XML Schema

### JSON Schema generation
- From MagicDraw in the containment browser, right-click the Data package.
- Select "Generate Ruby UML Metamodel"
  - You should get a message that the translation succeeded. The following warnings are expected in 19.0 SP4:
    - Unknown metaclass for stereotype additionalElementImport: ElementImport
    - Unknown metaclass for stereotype additionalPackageImport: PackageImport
    - Not including stereotype tag: UML Standard Profile::MagicDraw Profile::Info::version with value '"0.3.8"' applied on Gui_Builder_Profile
    -  The message will also specify validation warnings regarding names that don't "conform to convention". These warnings can be ignored
- A UMM file (.rb extension) will be generated in the ~/Prometheus/Generated_Code folder
- Run umm_json_schema on the UMM file
  ```bash
  umm_json_schema <path-to-UMM-file.rb> -o <output-json-schema-filename.json> -r id_attributes
  ``` 
- Check the current directory for the resulting JSON Schema (it will be named whatever you filled in for \<output-filename\> above.
